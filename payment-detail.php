<!DOCTYPE html>
<html>
	<head>
		<title>Beasty - Payment Detail</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, user-scalable=no">

		<?php include("partials/head.php") ?>
		
	</head>
	<body>
		<?php include("partials/top-navigation-dark.php") ?>
		<?php include("partials/side-navigation.php") ?>
		<?php include("partials/menu-navigation.php") ?>

		<div class="main-content">
			<section class="shipping-list">
                <div class="container beasty-wrapper">
                    <div class="box-head">
                        <h3 class="title">Payment Details</h3>
                    </div>
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Product Information</th>
                                    <th>Unit Price</th>
                                    <th>Quantity</th>
                                    <th>Subtotal</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>
                                        <div class="box-product">
                                            <div class="box-photo">
                                                <img src="assets/images/cart/Angus-Cuts-Cart.png" class="img-fluid" />
                                            </div>
                                            <p class="product-name">Angus Cuts</p>
                                        </div>
                                    </td>
                                    <td>$45.00</td>
                                    <td>1</td>
                                    <td>$45.00</td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="box-product">
                                            <div class="box-photo">
                                                <img src="assets/images/cart/Organ-Bites-Cart.png" class="img-fluid" />
                                            </div>
                                            <p class="product-name">Organ Bites</p>
                                        </div>
                                    </td>
                                    <td>$45.00</td>
                                    <td>2</td>
                                    <td>$90.00</td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="box-product">
                                            <div class="box-photo">
                                                <img src="assets/images/cart/Tendon-Chews-Cart.png" class="img-fluid" />
                                            </div>
                                            <p class="product-name">Tendon Chews</p>
                                        </div>
                                    </td>
                                    <td>$45.00</td>
                                    <td>2</td>
                                    <td>$90.00</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </section>

            <section class="shipping-progress">
                <div class="container beasty-wrapper">
                    <ul class="box-progress">
                        <li class="active">Shipping Details</li>
                        <li class="active">Payment Details</li>
                        <li>Order Confirmation</li>
                    </ul>
                    <div class="line"></div>
                </div>
            </section>

            <section class="shipping-order">
                <div class="container beasty-wrapper">
                    <div class="row payment-method">
                        <div class="col-md-6">
                            <h4 class="title">Payment Method</h4>
                            <div class="box-order box-head">
                                <p>Credit Card Via</p>
                                <img src="assets/images/payment/Stripe-Logo-Payment.png" class="img-fluid">
                            </div>
                            <div class="box-order">
                                <div class="box-form">
                                    <form action="" class="form grey">
                                        <div class="form-group">
                                            <label>Card Holder Namer</label>
                                            <input type="text" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label>Card Number</label>
                                            <input type="text" class="form-control">
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Expired Date</label>
                                                    <input type="text" class="form-control" />
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>CVV</label>
                                                    <input type="text" class="form-control" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="checkbox">
                                            <label><input type="checkbox" value="">Save payment information to my accountfor future purchases. </label>
                                         </div>
                                    </form>
                                </div>
                            </div>
                            <div class="box-note">
                                <div class="list-note">
                                    <div class="checkbox">
                                        <label><input type="checkbox" value="">Save payment information to my accountfor future purchases. </label>
                                    </div>
                                    <p>Your personal data will be used to process you order, support your experience throughout this website and for other purposes described in our privacy policy.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 order-summary">
                            <h4 class="title">Order Summary</h4>
                            <div class="box-order">
                                <div class="row">
                                    <div class="col-md-6">
                                        <p class="label">Items Subtotal</p>
                                    </div>
                                    <div class="col-md-6 text-right">
                                        <p>$ 225.00</p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <p class="label">Shipping</p>
                                    </div>
                                    <div class="col-md-6 text-right">
                                        <p>$ 0.00</p>
                                    </div>
                                </div>
                            </div>
                            <div class="box-order">
                                <div class="row">
                                    <div class="col-md-6">
                                        <p class="label"><b>Total</b></p>
                                    </div>
                                    <div class="col-md-6 text-right">
                                        <p><b>$ 225.00</b></p>
                                    </div>
                                </div>
                            </div>
                            <button class="btn btn-checkout" data-toggle="modal" data-target="#modalPaymentSuccess">Proceed to checkout</button>
                            <div class="box-note">
                                <div class="list-note">
                                    <p><b>Confirmation</b><p>
                                    <p>After purchasing a Pink Fable product we will send a confirmation message with delivery information and custom message template</p>
                                </div>
                                <div class="list-note">
                                    <p><b>Shipping and return policy</b><p>
                                    <p>We have a no refund policy. If goods are damaged during shipping we will replace and ship a new one free of charge.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
			
            <?php include("partials/footer.php") ?>
		</div>

        <!-- Modal Payment Success -->
        <div id="modalPaymentSuccess" class="modal fade modal-signin modal-success modal-payment-success" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-body">
                        <button type="button" class="close" data-dismiss="modal"><i class="fal fa-times"></i></button>
                        <div class="box-head">
                            <p class="title-form">Thank You</p>
                            <p class="title-form orange">For Your Order</p>
                        </div>
                        <div class="box-head small">
                            <p class="title-form">You successfully made </p>
                            <p class="title-form">a payment with Stripe.</p>
                        </div>
                        <div class="text-center btn-holder">
                            <a href="order-confirmation.php" class="btn btn-outline-primary text-dark">Order Confirmation</a>
                        </div>
                        <div class="box-image">
                            <img src="assets/images/payment/Success-Payment.png" class="img-fluid" />
                        </div>
                    </div>
                </div>

            </div>
        </div>

        <!-- Modal Payment Failed -->
        <div id="modalPaymentFailed" class="modal fade modal-signin modal-success modal-payment-success" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-body">
                        <button type="button" class="close" data-dismiss="modal"><i class="fal fa-times"></i></button>
                        <div class="box-head">
                            <p class="title-form orange">Opps, Sorry</p>
                            <p class="title-form">Payment Failed</p>
                        </div>
                        <div class="box-head small">
                            <p class="title-form">It looks like your order could not be proceed at this time. Please try again or select a different payment option</p>
                        </div>
                        <div class="text-center btn-holder">
                            <a href="#" class="btn btn-outline-primary text-dark">Try Again</a>
                        </div>
                    </div>
                </div>

            </div>
        </div>

        <?php include("partials/script.php") ?>

        <script>
                $(document).ready(function(){
                    $('.btn-show-address').click(function(){
                        $('.box-address').slideToggle();
                    });
                });
        </script>
	</body>
</html>