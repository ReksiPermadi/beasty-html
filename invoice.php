<!DOCTYPE html>
<html>
	<head>
		<title>Beasty - Invoice</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, user-scalable=no">

		<?php include("partials/head.php") ?>
		
	</head>
	<body>
		<div class="main-content">
			<div class="box-invoice">
                <div class="container beasty-wrapper">
                    <div class="box-head">
                        <div class="row">
                            <div class="col-md-6 col-6">
                                <div class="box-logo">
                                    <img src="assets/images/Beasty-Logo.svg" class="img-fluid" />
                                </div>
                            </div>
                            <div class="col-md-6 col-6 text-right">
                                <h4 class="title"><span>Your</span> Invoice</h4>
                            </div>
                        </div>
                    </div>
                    <div class="box-content">
                        <h3 class="order-number">Order Number : #547856</h3>
                        <div class="box-info-customer">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="list-info">
                                        <div class="row">
                                            <div class="col-md-4 col-4">
                                                <p class="label">Date</p>
                                            </div>
                                            <div class="col-md-8 col-8">
                                                <p>1 September 2020</p>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4 col-4">
                                                <p class="label">Payment</p>
                                            </div>
                                            <div class="col-md-8 col-8">
                                                <p>Credit Card via Stripe</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="list-info">
                                        <div class="row">
                                            <div class="col-md-4 col-4">
                                                <p class="label">DELIVERY<br/>ADDRESS</p>
                                            </div>
                                            <div class="col-md-8 col-8">
                                                <p>Christopher Brian<br/>+8075435432<br/>7/25 Adelaide St, BrisbaneCity, Queensland, Australia, 4000</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="table-invoice">
                            <div class="list-data">
                                <p><b>Payment Details (1 Invoice)</b></p>
                            </div>
                            <div class="list-data">
                                <p>Total Order</p>
                                <br/>
                                <div class="row">
                                    <div class="col-md-6 col-9">
                                        <p>- BEASTY Angus Cuts</p>
                                        <p>- BEASTY Organ Bites</p>
                                        <p>- BEASTY Tendon Chews</p>
                                    </div>
                                    <div class="col-md-3 quantity">
                                        <p>1 product</p>
                                        <p>2 products</p>
                                        <p>2 products</p>
                                    </div>
                                    <div class="col-md-3 col-3 text-right">
                                        <p>$45</p>
                                        <p>$90</p>
                                        <p>$90</p>
                                    </div>
                                </div>
                            </div>
                            <div class="list-data">
                                <div class="row">
                                    <div class="col-md-6 col-6">
                                        <p><b>Sub-Total</b></p>
                                    </div>
                                    <div class="col-md-6 col-6 text-right">
                                        <p><b>$225</b></p>
                                    </div>
                                </div>
                            </div>
                            <div class="box-total">
                                <div class="row justify-content-end">
                                    <div class="col-md-6 col-10">
                                        <div class="list-data">
                                            <div class="row">
                                                <div class="col-md-8 col-8">
                                                    <p>DHL Express Shipping</p>
                                                </div>
                                                <div class="col-md-4 col-4 text-right">
                                                    <p>$100</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="list-data">
                                            <div class="row">
                                                <div class="col-md-8 col-8">
                                                    <p><b>Sub-Total</b></p>
                                                </div>
                                                <div class="col-md-4 col-4 text-right">
                                                    <p><b>$325</b></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
		</div>

        <?php include("partials/script.php") ?>
	</body>
</html>