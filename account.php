<!DOCTYPE html>
<html>
	<head>
		<title>Beasty - My Account</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, user-scalable=no">

		<?php include("partials/head.php") ?>
		
	</head>
	<body>
		<?php include("partials/top-navigation-dark.php") ?>
		<?php include("partials/side-navigation.php") ?>
		<?php include("partials/menu-navigation.php") ?>

		<div class="main-content">
			<section class="section-account">
                <div class="container beasty-wrapper">
                    <div class="row">
                        <div class="col-lg-4 col-md-12">
                            <h3 class="title">My Account</h3>
                        </div>
                        <div class="col-lg-8 col-md-12">
                            <ul class="nav nav-tabs">
                                <li>
                                    <a data-toggle="tab" href="#myprofile" class="active">My Profile</a>
                                </li>
                                <li>
                                    <a data-toggle="tab" href="#myaddress">My Address</a>
                                </li>
                                <li>
                                    <a data-toggle="tab" href="#orderlist">Order List</a>
                                </li>
                                <li>
                                    <a data-toggle="tab" href="#petprofile">Pet Profile</a>
                                </li>
                                <li>
                                    <a href="#">Sign Out</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="tab-content">
                        <div id="myprofile" class="tab-pane fade in active show">
                            <?php include("partials/account/my-profile.php") ?>
                        </div>
                        <div id="myaddress" class="tab-pane fade">
                            <?php include("partials/account/my-address.php") ?>
                        </div>
                        <div id="orderlist" class="tab-pane fade">
                            <?php include("partials/account/order-list.php") ?>
                        </div>
                        <div id="petprofile" class="tab-pane fade">
                            <?php include("partials/account/pet-profile.php") ?>
                        </div>
                    </div>
                </div>
            </section>
			
			<?php include("partials/footer.php") ?>
		</div>

        <?php include("partials/script.php") ?>
        
        <script>
            $(document).ready(function(){
                // My Profile
                $('.btn-edit-profile').click(function(){
                    $('.box-profile').hide();
                    $('.edit-profile').fadeIn();
                });

                $('.btn-save-profile').click(function(){
                    $('.edit-profile').hide();
                    $('.reset-password').hide();
                    $('.box-profile').fadeIn();
                });

                $('.btn-reset-password').click(function() {
                    $('.box-profile').hide();
                    $('.reset-password').fadeIn();
                });

                // My Address
                $('.btn-edit-address').click(function(){
                    $('.box-list-address').hide();
                    $('.edit-address').fadeIn();
                });

                $('.btn-add-address').click(function(){
                    $('.box-list-address').hide();
                    $('.add-address').fadeIn();
                });

                $('.btn-save-address').click(function(){
                    $('.edit-address').hide();
                    $('.add-address').hide();
                    $('.box-list-address').fadeIn();
                });

                // Order List
                $('.btn-track-order').click(function(){
                    $('.box-order-list').hide();
                    $('.box-tracking').fadeIn();
                });

                $('.btn-back-order').click(function(){
                    $('.box-tracking').hide();
                    $('.box-order-list').fadeIn();
                });

                //Pet Profile
                $('.upload-photo').change(function(){
                    if (this.files && this.files[0]) {
                        var reader = new FileReader();
                        reader.onload = function (e) {
                            $('.box-image-preview').attr('style', 'background-image: url("' + e.target.result + '")');
                        }
                        reader.readAsDataURL(this.files[0]);
                    }
                });

                $('.favourite').selectize({
                    plugins: ['remove_button'],
                    maxItems: null,
                    valueField: 'id',
                    labelField: 'title',
                    searchField: 'title',
                    create: false
                });

                $('.btn-new-pet').click(function(){
                    $('.box-pet-profile').hide();
                    $('.box-add-pet').fadeIn();
                });

                $('.btn-add-pet').click(function(){
                    $('.box-add-pet').hide();
                    $('.box-pet-profile').fadeIn(); 
                });

                $('.btn-edit-pet').click(function(){
                    $('.box-pet-profile').hide(); 
                    $('.box-edit-pet').fadeIn();
                });

                $('.btn-save-pet').click(function(){
                    $('.box-edit-pet').hide();
                    $('.box-pet-profile').fadeIn(); 
                });
            });
        </script>

	</body>
</html>