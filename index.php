<!DOCTYPE html>
<html>
	<head>
		<title>Beasty</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, user-scalable=no">

		<?php include("partials/head.php") ?>
		
	</head>
	<body>
		<?php include("partials/top-navigation.php") ?>
		<?php include("partials/side-navigation.php") ?>
		<?php include("partials/menu-navigation.php") ?>

		<div class="main-content" id="home">
			<section class="section section-banner-home">
				<div class="container beasty-wrapper">
					<div class="box-content">
						<div class="content-banner">
							<div class="small-title">
								<h3>Feed EM</h3>
							</div>
							<div class="big-title">
								<h1>Wild</h1>
							</div>
							<div class="description-banner">
								<p>You eat clean, shouldn't you<br/>feed your companion the same ?</p>
							</div>
						</div>
						<div class="img-banner">
							<img src="assets/images/homepage/img-1.png" class="img-fluid"/>
						</div>
					</div>
				</div>
			</section>

			<section class="section section-product-slide fp-auto-height">
				<div class="product-slider owl-carousel owl-theme">
					<div class="product-item" style="background: url(assets/images/homepage/background-product-slider1.jpg) no-repeat top; background-size: cover;">
						<div class="container beasty-wrapper">
							<div class="box-content">
								<div class="row">
									<div class="col-lg-6 col-md-12 order-last order-lg-first">
										<div class="box-image-product">
											<img src="assets/images/homepage/product/Angus-Cuts.png" class="img-fluid">
										</div>
										<div class="btn-holder text-center btn-mobile">
											<a href="product-detail-angus-cuts.php" class="btn btn-outline-primary">View Product</a>
										</div>
									</div>
									<div class="col-lg-6 col-md-12 order-first order-lg-last">
										<div class="box-description-product">
											<p class="slogan-product">FREEZE-DRIED 100% GRASS-FED BEEF</p>
											<h2 class="name-product">Angus Cuts</h2>
											<p class="description-product">
												Beasty Angus Cuts are made from the hind which is the leanest and most muscular part of the cow
											</p>
											<a href="product-detail-angus-cuts.php" class="btn btn-outline-primary">View Product</a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="product-item" style="background: url(assets/images/homepage/background-product-slider2.jpg) no-repeat top; background-size: cover;">
						<div class="container beasty-wrapper">
							<div class="box-content">
								<div class="row">
									<div class="col-lg-6 col-md-12 order-last order-lg-first">
										<div class="box-image-product">
											<img src="assets/images/homepage/product/Organ-Bites.png" class="img-fluid">
										</div>
										<div class="btn-holder text-center btn-mobile">
											<a href="product-detail-organ-bites.php" class="btn btn-outline-primary">View Product</a>
										</div>
									</div>
									<div class="col-lg-6 col-md-12 order-first order-lg-last">
										<div class="box-description-product">
											<p class="slogan-product">FREEZE-DRIED 100% GRASS-FED BEEF</p>
											<h2 class="name-product">Organ Bites</h2>
											<p class="description-product">
											Beasty Organ Bites are made to emulate the natural wild diet where the entire animal, including organs, would be consumed. 
											</p>
											<a href="product-detail-organ-bites.php" class="btn btn-outline-primary">View Product</a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="product-item" style="background: url(assets/images/homepage/background-product-slider3.jpg) no-repeat top; background-size: cover;">
						<div class="container beasty-wrapper">
							<div class="box-content">
								<div class="row">
									<div class="col-lg-6 col-md-12 order-last order-lg-first">
										<div class="box-image-product">
											<img src="assets/images/homepage/product/Tendon-Chews.png" class="img-fluid">
										</div>
										<div class="btn-holder text-center btn-mobile">
											<a href="product-detail-tendon-chews.php" class="btn btn-outline-primary">View Product</a>
										</div>
									</div>
									<div class="col-lg-6 col-md-12 order-first order-lg-last">
										<div class="box-description-product">
											<p class="slogan-product">FREEZE-DRIED 100% GRASS-FED BEEF</p>
											<h2 class="name-product">Tendon Chews</h2>
											<p class="description-product">
												Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a. Nulla quis lorem ut libero malesuada feugiat.
											</p>
											<a href="product-detail-tendon-chews.php" class="btn btn-outline-primary">View Product</a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="product-item" style="background: url(assets/images/homepage/background-product-slider4.jpg	) no-repeat top; background-size: cover;">
						<div class="container beasty-wrapper">
							<div class="box-content">
								<div class="row">
									<div class="col-lg-6 col-md-12 order-last order-lg-first">
										<div class="box-image-product">
											<img src="assets/images/homepage/product/Wagyu-Cuts.png" class="img-fluid">
										</div>
										<div class="btn-holder text-center btn-mobile">
											<a href="product-detail-wagyu-cuts.php" class="btn btn-outline-primary">View Product</a>
										</div>
									</div>
									<div class="col-lg-6 col-md-12 order-first order-lg-last">
										<div class="box-description-product">
											<p class="slogan-product">FREEZE-DRIED 100% GRASS-FED BEEF</p>
											<h2 class="name-product"><b>W</b>agyu Cuts</h2>
											<p class="description-product">
												Nulla quis lorem ut libero malesuada feugiat. Curabitur aliquet quam id dui posuere blandit. 
											</p>
											<a href="product-detail-wagyu-cuts.php" class="btn btn-outline-primary">View Product</a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="row box-dots">
					<div class="col-lg-6"></div>
					<div class="col-lg-6 col-md-12 order-2">
						<div class="product-dots-container"></div>
					</div>
				</div>
			</section>

			<section class="section section-story-home">
				<div class="container beasty-wrapper">
					<div class="box-story">
						<div class="row">
							<div class="col-lg-6 col-md-12">
								<div class="story-content">
									<h4 class="small-title">Made From</h4>
									<h3 class="big-title">The Best</h3>
									<p class="subtitle">Mother Nature Offers</p>
									<p class="description">
										Beasty by Crown State Pastoral was created by
										a family of farmers with a love for pets. Located in
										New South Wales Australia, we have been
										raisingaward-winning cattle for the past 10 years. 
									</p>
									<a href="our-story.php" class="btn btn-outline-secondary ph-2">Our Story</a>
								</div>
							</div>
							<div class="col-lg-6 col-md-12">
								<div class="box-image">
									<img src="assets/images/homepage/story/icon-story.png" class="img-fluid" />
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>

			<!-- <section class="section section-latest-news">
				<div class="container beasty-wrapper">
					<div class="box-head text-center">
						<h3 class="title-small">Latest News</h3>
					</div>
					<div class="latest-news owl-carousel">
						<div class="item-news" style="background: url(assets/images/homepage/news/Latest-News-1.jpg); background-size: cover;">
							<div class="box-image">
								<img src="assets/images/homepage/news/Latest-News-1.jpg" class="img-fluid" />
							</div>
							<div class="content-news">
								<h5 class="title-news">Preparing your dog for<br/> a change  in routine</h5>
								<a href="article-detail.php" class="link-more">read more</a>
							</div>
						</div>
						<div class="item-news" style="background: url(assets/images/homepage/news/Latest-News-2.jpg); background-size: cover;">
							<div class="box-image">
								<img src="assets/images/homepage/news/Latest-News-2.jpg" class="img-fluid" />
							</div>
							<div class="content-news">
								<h5 class="title-news">A healthy dog is<br/> an active dog</h5>
								<a href="article-detail.php" class="link-more">read more</a>
							</div>
						</div>
						<div class="item-news" style="background: url(assets/images/homepage/news/Latest-News-3.jpg); background-size: cover;">
							<div class="box-image">
								<img src="assets/images/homepage/news/Latest-News-3.jpg" class="img-fluid" />
							</div>
							<div class="content-news">
								<h5 class="title-news">Heeping your best friend<br/> happy with beasty</h5>
								<a href="article-detail.php" class="link-more">read more</a>
							</div>
						</div>
						<div class="item-news" style="background: url(assets/images/homepage/news/Latest-News-1.jpg); background-size: cover;">
							<div class="box-image">
								<img src="assets/images/homepage/news/Latest-News-1.jpg" class="img-fluid" />
							</div>
							<div class="content-news">
								<h5 class="title-news">Preparing your dog for a change  in routine</h5>
								<a href="article-detail.php" class="link-more">read more</a>
							</div>
						</div>
						<div class="item-news" style="background: url(assets/images/homepage/news/Latest-News-2.jpg); background-size: cover;">
							<div class="box-image">
								<img src="assets/images/homepage/news/Latest-News-2.jpg" class="img-fluid" />
							</div>
							<div class="content-news">
								<h5 class="title-news">A healthy dog is an active dog</h5>
								<a href="article-detail.php" class="link-more">read more</a>
							</div>
						</div>
						<div class="item-news" style="background: url(assets/images/homepage/news/Latest-News-3.jpg); background-size: cover;">
							<div class="box-image">
								<img src="assets/images/homepage/news/Latest-News-3.jpg" class="img-fluid" />
							</div>
							<div class="content-news">
								<h5 class="title-news">Heeping your best friend happy with beasty</h5>
								<a href="article-detail.php" class="link-more">read more</a>
							</div>
						</div>
					</div>
					<div class="text-center">
						<a href="article.php" class="btn btn-outline-primary">Show More</a>
					</div>
				</div>
			</section> -->
			
			<?php include("partials/footer.php") ?>
		</div>

		<?php include("partials/script.php") ?>
		<script>
			$(document).ready(function(){
				var home = $('#home').fullpage({
					navigation: true,
					navigationPosition: 'right',
					bigSectionsDestination: 'top',
					onLeave: function(index, nextIndex, direction){     
						if(nextIndex == 1){
							$('.top-nav').removeClass('with-bg');
						}else{
							$('.top-nav').addClass('with-bg');     
						}

						if(nextIndex == 3 || nextIndex == 4) {
							$('.menu-desktop .account-menu img').attr('src','assets/images/side-menu/Account-Icon-Dark.svg');
							$('.menu-desktop .shopping-cart-menu img').attr('src','assets/images/side-menu/Shopping-Cart-Icon-Dark.svg');
							$('.menu-desktop .product-menu img').attr('src','assets/images/side-menu/Product-Catalogue-Icon-Dark.svg');
							$('.menu-desktop .our-story-menu img').attr('src','assets/images/side-menu/Our-Story-Icon-Dark.svg');
						} else {
							$('.menu-desktop .account-menu img').attr('src','assets/images/side-menu/Account-Icon.svg');
							$('.menu-desktop .shopping-cart-menu img').attr('src','assets/images/side-menu/Shopping-Cart-Icon.svg');
							$('.menu-desktop .product-menu img').attr('src','assets/images/side-menu/Product-Catalogue-Icon.svg');
							$('.menu-desktop .our-story-menu img').attr('src','assets/images/side-menu/Our-Story-Icon.svg');
						}
					},
					// responsiveWidth: 767,
					// afterResponsive: function(isResponsive){
						
					// }
				});
			});
		</script>

	</body>
</html>