// This is main js for custom
var productDetail = $('#product-detail').fullpage({
    navigation: true,
    navigationPosition: 'right',
    onLeave: function(index, nextIndex, direction){        
        if(nextIndex == 1){
            $('.top-nav').removeClass('with-bg');
        }else{
            $('.top-nav').addClass('with-bg');     
        }

        if(nextIndex == 2) {
            $('#fp-nav').addClass('dark');
            $('.menu-desktop .account-menu img').attr('src','assets/images/side-menu/Account-Icon-Dark.svg');
            $('.menu-desktop .shopping-cart-menu img').attr('src','assets/images/side-menu/Shopping-Cart-Icon-Dark.svg');
            $('.menu-desktop .product-menu img').attr('src','assets/images/side-menu/Product-Catalogue-Icon-Dark.svg');
            $('.menu-desktop .our-story-menu img').attr('src','assets/images/side-menu/Our-Story-Icon-Dark.svg'); 
        } else {
            $('#fp-nav').removeClass('dark');
            $('.menu-desktop .account-menu img').attr('src','assets/images/side-menu/Account-Icon.svg');
            $('.menu-desktop .shopping-cart-menu img').attr('src','assets/images/side-menu/Shopping-Cart-Icon.svg');
            $('.menu-desktop .product-menu img').attr('src','assets/images/side-menu/Product-Catalogue-Icon.svg');
            $('.menu-desktop .our-story-menu img').attr('src','assets/images/side-menu/Our-Story-Icon.svg');
        }
     }
});

// Owl Product Home
setTimeout(function(){ 
    $('.product-slider').owlCarousel({
        loop: true,
        dots: true,
        dotsContainer: '.product-dots-container',
        items: 1,
        nav: true,
        navText:["<div class='nav-btn prev-slide'><img src='assets/images/Arrow-Slider.svg' class='img-fluid' /></div>","<div class='nav-btn next-slide'><img src='assets/images/Arrow-Slider.svg' class='img-fluid' /></div>"],
    });
}, 1000);

if($(window).width() > 769) {
    $('.latest-news.owl-carousel').owlCarousel({
        loop: false,
        dots: false,
        items: 3,
        nav: true,
        navText:["<div class='nav-btn prev-slide'><img src='assets/images/Arrow-Slider.svg' class='img-fluid' /></div>","<div class='nav-btn next-slide'><img src='assets/images/Arrow-Slider.svg' class='img-fluid' /></div>"],
    });
}

$('.navbar-toggle').click(function(){
    $(this).addClass('hide');
    $('.box-menu').addClass('show');
});

$('.btn-close i').click(function(){
    $('.box-menu').removeClass('show');
    $('.navbar-toggle').removeClass('hide');
});

$('.next-signup').click(function(){
    $('.first-step').hide();
    $('.next-step').fadeIn();
});

$('.back-signup').click(function(){
    $('.next-step').hide();
    $('.first-step').fadeIn();
});

$('.btn-signup').click(function() {
    $('#modalSignin').modal('hide');
    $('#modalSuccess').modal('show');
});

$('.forgot-password').click(function() {
    $('#modalSignin').modal('hide');
    $('#modalForgotPassword').modal('show');
});

$('.btn-cart').click(function(){
    $('.modal-backdrop').addClass('show');
    $('.box-cart').addClass('active');
    $('html').addClass('cart-open');
    $('body').addClass('cart-open');
    $.fn.fullpage.setAutoScrolling(false);
});

$('.btn-close-cart').click(function(){
    $('.box-cart').removeClass('active');
    $('.modal-backdrop').removeClass('show');
    $('html').removeClass('cart-open');
    $('body').removeClass('cart-open');
    $.fn.fullpage.setAutoScrolling(true);
});

// if (window.matchMedia('(max-width: 767px)').matches) {
//     setTimeout(function(){ 
//         $.fn.fullpage.setAutoScrolling(false);
//     }, 2000);
// } else {
//     setTimeout(function(){ 
//         $.fn.fullpage.setAutoScrolling(true);
//     }, 2000);
// }

// function add quantity
$('.btn-plus').click(function(){
    var count = $(this).parent().find('.quantity').val();
    count++;
    $(this).parent().find('.quantity').val(count);
});

$('.btn-min').click(function(){
    var count = $(this).parent().find('.quantity').val();
    if (count > 1) {
        count--;
        $(this).parent().find('.quantity').val(count);
    }
});

$('.btn-plus-calculate').click(function(){
    var count = $(this).parent().find('.quantity').val();
    count++;
    $(this).parent().find('.quantity').val(count);
    $(this).parent().find('.total-price span').html(count * $(this).parent().parent().find('.price-product b span').html() + '.00');
});

$('.btn-min-calculate').click(function(){
    var count = $(this).parent().find('.quantity').val();
    if (count > 1) {
        count--;
        $(this).parent().find('.quantity').val(count);
        $(this).parent().find('.total-price span').html(count * $(this).parent().parent().find('.price-product b span').html() + '.00');
    }
});

$('.woocommerce-NoticeGroup').addClass('container beasty-wrapper');

$(window).scroll(function() {
    var nav = $('.top-nav');
    var top = 200;
    if ($(window).scrollTop() >= top) {
        nav.addClass('with-bg');
    } else {
        nav.removeClass('with-bg');
    }
});