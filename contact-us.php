<!DOCTYPE html>
<html>
	<head>
		<title>Beasty - Contact Us</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, user-scalable=no">

		<?php include("partials/head.php") ?>
		
	</head>
	<body>
		<?php include("partials/top-navigation.php") ?>
		<?php include("partials/side-navigation.php") ?>
		<?php include("partials/menu-navigation.php") ?>

		<div class="main-content">
            <section class="section-banner-contact" style="background-image: url(assets/images/contact/Dog-Contact-Us.png), url(assets/images/contact/Yellow-Background-Contact.jpg);">
                <div class="container beasty-wrapper">
                    <div class="row justify-content-end">
                        <div class="col-lg-6 col-md-12">
                            <div class="box-content">
                                <p class="small-title">Get In Touch</p>
                                <h3 class="title">With Us</h3>
                                <div class="box-address">
                                    <p class="company-name">Crown State Pastoral Company</p>
                                    <p class="address">1501 A, Level 15, Tower 1, Westfield
                                    520 Oxford Street, Bondi Junction NSW
                                    2022, Australia. </p>
                                    <p class="phone">+61 (0)2 8305 0300</p>
                                </div>  
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section class="section-maps">
                <div class="row">
                    <div class="col-lg-6 col-md-12">
                        <div class="box-form">
                            <div class="box-head">
                                <p class="small-title">Send Us A</p>
                                <p class="title">Message</p>
                            </div>
                            <form action="" class="form">
                                <div class="form-group">
                                    <label>Name</label>
                                    <input type="text" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label>Phone</label>
                                    <input type="number" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label>Email</label>
                                    <input type="email" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label>Message</label>
                                    <textarea class="form-control"></textarea>
                                </div>
                                <div class="text-center btn-holder">
                                    <button type="submit" class="btn btn-outline-primary text-dark">Submit</button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-12">
                        <div class="box-maps">
                            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3311.9161151184576!2d151.24839171477015!3d-33.89181418064982!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x6b12adf2279c7f49%3A0x2fa7efc59e406791!2s520%20Oxford%20St%2C%20Bondi%20Junction%20NSW%202022%2C%20Australia!5e0!3m2!1sid!2sid!4v1599669348493!5m2!1sid!2sid" width="100%" height="100%" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                        </div>
                    </div>  
                </div>
            </section>
			
			<?php include("partials/footer.php") ?>
		</div>

        <?php include("partials/script.php") ?>
	</body>
</html>