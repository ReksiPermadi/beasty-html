<!DOCTYPE html>
<html>
	<head>
		<title>Beasty - Product - Angus Cuts</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, user-scalable=no">

		<?php include("partials/head.php") ?>
		
	</head>
	<body>
		<?php include("partials/top-navigation.php") ?>
		<?php include("partials/side-navigation.php") ?>
		<?php include("partials/menu-navigation.php") ?>

		<div class="main-content" id="product-detail">
			<section class="section section-banner-product-detail">
				<div class="container beasty-wrapper">
					<div class="box-content">
						<div class="box-head mobile">
							<div class="small-title">
								<h3>FREEZE DRIED 100% GRASS-FED BEEF</h3>
							</div>
							<div class="big-title">
								<h1>Angus Cuts</h1>
							</div>
						</div>
                        <div class="img-banner">
							<img src="assets/images/product/product-detail/angus-cuts/Angus-Cuts-Detail-Produt.png" class="img-fluid"/>
						</div>
						<div class="content-banner">
							<div class="box-head desktop">
								<div class="small-title">
									<h3>FREEZE DRIED 100% GRASS-FED BEEF</h3>
								</div>
								<div class="big-title">
									<h1>Angus Cuts</h1>
								</div>
							</div>
							<div class="box-info">
                                <p class="price-product"><b>$45</b> | 3 oz (85 g)</p>
                                <p class="desc-product">Beasty Angus Cuts are made from the hind which is the leanest and most muscular part of the cow</p>
                                <div class="box-form form-inline box-quantity">
                                    <button class="btn btn-oval btn-min"><i class="fas fa-minus"></i></button>
                                    <input type="number" class="form-control number quantity" value="1" />
                                    <button class="btn btn-oval btn-plus"><i class="fas fa-plus"></i></button>
                                    <div class="text-right">
                                        <button class="btn btn-outline-primary text-dark">Add to Cart</button>
                                    </div>
                                </div>
                            </div>
						</div>
					</div>
				</div>
			</section>

			<section class="section section-quality">
				<div class="container beasty-wrapper">
					<div class="head-title">
						<h2 class="title">Human Quality</h2>
						<p class="subtitle">Standard to pet food</p>
					</div>
					<div class="row">
						<div class="col-lg-8 col-md-12 order-lg-first order-last">
							<div class="box-quality">
								<div class="row">
									<div class="col-md-4 col-4">
										<div class="list-quality quality1">
											<p class="title-quality">Whole Food</p>
											<p class="desc-quality">that are unprocessed</p>
										</div>
									</div>
									<div class="col-md-4 col-4">
										<div class="list-quality quality2">
											<p class="title-quality">Natural</p>
											<p class="desc-quality">and fresh produce</p>
										</div>
									</div>
									<div class="col-md-4 col-4">
										<div class="list-quality quality3">
											<p class="title-quality">NON-GMO</p>
											<p class="desc-quality">and Hormone Free</p>
										</div>
									</div>
								</div>
								<div class="row bottom">
									<div class="col-md-4 col-4 mx-auto">
										<div class="list-quality quality4">
											<p class="title-quality">FARM to bowl</p>
											<p class="desc-quality">sourcing for your pet.</p>
										</div>
									</div>
									<div class="col-md-4 col-4 mx-auto">
										<div class="list-quality quality5">
											<p class="title-quality">KEPT FRESH</p>
											<p class="desc-quality">with the best technology</p>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-lg-4 col-md-12 order-lg-last order-first">
							<div class="box-image">
								<img src="assets/images/product/product-detail/angus-cuts/Angus-Cuts-2-Detail-Product.png" class="img-fluid" />
							</div>
						</div>
					</div>
				</div>
			</section>

			<section class="section section-ingridient">
				<div class="container beasty-wrapper">
					<div class="row">
						<div class="col-lg-6 col-md-12 order-lg-first order-last">
							<div class="box-hastag hastag-desktop">
								<p>#Gobeasty</p>
							</div>
							<div class="box-image">
								<img src="assets/images/product/product-detail/Dog-Product-Detail.png" class="img-fluid" />
							</div>
						</div>
						<div class="col-lg-6 col-md-12 order-lg-last order-first">
							<div class="box-hastag hastag-mobile">
								<p>#Gobeasty</p>
							</div>
							<ul class="box-category">
								<li class="list-category">Whole Food</li>
								<li class="list-category">Single Source</li>
								<li class="list-category">Grass Fed</li>
								<li class="list-category">Grain Free</li>
							</ul>
							<div class="box-ingridient">
								<div class="row">
									<div class="col-md-6 col-6">
										<div class="list-ingridient">
											<p>Crude Protein</p>
											<span>(min)</span>
											<p class="percent">65%</p>
										</div>
									</div>
									<div class="col-md-6 col-6">
										<div class="list-ingridient">
											<p>Crude Fat</p>
											<span>(min)</span>
											<p class="percent">10%</p>
										</div>
									</div>
									<div class="col-md-6 col-6">
										<div class="list-ingridient">
											<p>Crude Fiber</p>
											<span>(max)</span>
											<p class="percent">1%</p>
										</div>
									</div>
									<div class="col-md-6 col-6">
										<div class="list-ingridient">
											<p>Moister</p>
											<span>(max)</span>
											<p class="percent">5%</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>

			<section class="section section-recomendation">
				<div class="container beasty-wrapper">
					<div class="box-content">
						<h2 class="title">Feeding</h2>
						<h3 class="subtitle">Recomendations</h3>
						<p class="description">
							Beasty's Freeze-Dried Angus Cuts are designed to be a treat or rehydrated in water as a food topper. This treat is not intended to be a balanced diet. Not for Human Consumption. Always provide plenty of fresh water for your dog.
						</p>
					</div>
				</div>
			</section>

			<section class="section section-more-product">
				<div class="container beasty-wrapper">
					<div class="box-head text-center">
						<h3 class="title-small">More for your dog</h3>
					</div>
					<div class="row">
						<div class="col-md-4">
							<div class="list-more-product">
								<img src="assets/images/product/product-detail/More-Organ-Bites-Product-Detail.png" class="img-fluid" /> 
								<div class="box-info">
									<p class="title-product">Organ Bites</p>
									<a href="product-detail-organ-bites.php" class="btn btn-outline-primary">View Product</a>
								</div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="list-more-product">
								<img src="assets/images/product/product-detail/More-Wagyu-Cuts-Product-Detail.png" class="img-fluid" /> 
								<div class="box-info">
									<p class="title-product">Wagyu Cuts</p>
									<a href="product-detail-wagyu-cuts.php" class="btn btn-outline-primary">View Product</a>
								</div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="list-more-product">
								<img src="assets/images/product/product-detail/More-Tendon-Chews-Product-Detail.png" class="img-fluid" /> 
								<div class="box-info">
									<p class="title-product">Tendon Chews</p>
									<a href="product-detail-tendon-chews.php" class="btn btn-outline-primary">View Product</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
			
			<?php include("partials/footer.php") ?>
		</div>

		<?php include("partials/script.php") ?>
	</body>
</html>