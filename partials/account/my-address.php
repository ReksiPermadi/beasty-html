<div class="box-list-address">
    <div class="row">
        <div class="col-lg-4 col-md-12">
            <div class="box-head">
                <p class="title-form">My<br/>Address</p>
            </div>
        </div>
        <div class="col-lg-8 col-md-12">
            <div class="table-responsive">
                <table class="table">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Address</th>
                            <th>Phone</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>Christopher Brian <b>[Default]</b></td>
                            <td>7/25 Adelaide St, Brisbane City, QLD, Australia, 4000</td>
                            <td>+8075092299</td>
                            <td>
                                <a href="#" class="btn-edit-address"><i class="far fa-edit"></i></a>
                                <a href="#"><i class="far fa-trash-alt"></i></a>
                            </td>
                        </tr>
                        <tr>
                            <td>Felix Brian Denise</td>
                            <td>346 Panorama Ave, Bathurst, NSW, Australia, 2795</td>
                            <td>+8075435432</td>
                            <td>
                                <a href="javascript:void(0)" class="btn-edit-address"><i class="far fa-edit"></i></a>
                                <a href="#"><i class="far fa-trash-alt"></i></a>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="btn-holder">
                <button class="btn btn-outline-primary text-dark btn-add-address">Add Address</button>
            </div>
        </div>
    </div>
</div>

<div class="add-address">
    <div class="row">
        <div class="col-lg-4 col-md-12">
            <div class="box-head">
                <p class="title-form">Add New<br/>Address</p>
            </div>
        </div>
        <div class="col-lg-8 col-md-12">
            <form action="" class="form address">
                <div class="form-group">
                    <label>Name</label>
                    <input type="text" class="form-control" />
                </div>
                <div class="form-group">
                    <label>Phone Number</label>
                    <input type="text" class="form-control" />
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Country</label>
                            <input type="text" class="form-control" />
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Region</label>
                            <select class="form-control">
                                <option value="0">Select Region</option>
                                <option value="1">Queensland</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Postal Code</label>
                            <input type="text" class="form-control" />
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label>Address</label>
                    <textarea class="form-control"></textarea>
                </div>
            </form>
            <div class="btn-holder">
                <button class="btn btn-outline-primary text-dark btn-save-address">Add Address</button>
            </div>
        </div>
    </div>
</div>

<div class="edit-address">
    <div class="row">
        <div class="col-lg-4 col-md-12">
            <div class="box-head">
                <p class="title-form">Edit<br/>Address</p>
            </div>
        </div>
        <div class="col-lg-8 col-md-12">
            <form action="" class="form address">
                <div class="form-group">
                    <label>Name</label>
                    <input type="text" class="form-control" value="Christopher Brian" />
                </div>
                <div class="form-group">
                    <label>Phone Number</label>
                    <input type="text" class="form-control" value="+8075092299" />
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Country</label>
                            <input type="text" class="form-control" value="Australia" />
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Region</label>
                            <select class="form-control">
                                <option value="0">Select Region</option>
                                <option value="1" selected>Queensland</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Postal Code</label>
                            <input type="text" class="form-control" value="4000" />
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="address">Address</label>
                    <textarea class="form-control" id="address">
7/25 Adelaide St
Brisbane City,
Queensland, Australia</textarea>
                </div>
            </form>
            <div class="btn-holder">
                <button class="btn btn-outline-primary text-dark btn-save-address">Save Changes</button>
            </div>
        </div>
    </div>
</div>
