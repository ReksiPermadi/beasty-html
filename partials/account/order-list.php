<div class="box-order-list">
    <div class="row">
        <div class="col-lg-4 col-md-12">
            <div class="box-head">
                <p class="title-form">Order<br/>List</p>
            </div>
        </div>
        <div class="col-lg-8 col-md-12">
            <div class="box-order">
                <div class="box-head">
                    <div class="row justify-content-between">
                        <div class="col-lg-4 col-md-6 col-6">
                            <p>01/09/2020</p>
                        </div>
                        <div class="col-lg-4 col-md-6 col-6">
                            <p>DHL Express Shiping</p>
                        </div>
                        <div class="col-lg-4 col-md-12 col-12">
                            <p><b>Receipt Number</b></p>
                            <p>0888297732</p>
                        </div>
                    </div>
                </div>
                <div class="box-order-desc">
                    <div class="row order-info-desk">
                        <div class="col-md-4 col-4">
                            <div class="box-desc">
                                <p>Order Number</p>
                                <p><b>#547856</b> | Print</p>
                            </div>
                        </div>
                        <div class="col-md-4 col-4">
                            <div class="box-desc">
                                <p>Status</p>
                                <p><b>In-Delivery</b></p>
                            </div>
                        </div>
                        <div class="col-md-4 col-4">
                            <div class="box-desc">
                                <p>Total Price</p>
                                <p><b>$225.00</b></p>
                            </div>
                        </div>
                    </div>
                    <div class="row order-info-mobile">
                        <div class="col-md-12">
                            <div class="box-desc">
                                <div class="row">
                                    <div class="col-md-6 col-6"><p>Order Number</p></div>
                                    <div class="col-md-6 col-6"><p><b>#547856</b> | Print</p></div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6 col-6"><p>Status</p></div>
                                    <div class="col-md-6 col-6"><p><b>In-Delivery</b></p></div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6 col-6"><p>Total Price</p></div>
                                    <div class="col-md-6 col-6"><p><b>$225.00</b></p></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box-list">
                    <div class="order-list">
                        <div class="row">
                            <div class="col-md-4 col-4 my-auto">
                                <div class="box-content text-center">
                                    <img src="assets/images/account/Angus-Cuts-Order-List-Account.png" class="img-fluid"/>
                                </div>
                            </div>
                            <div class="col-md-4 col-4 my-auto">
                                <div class="box-content">
                                    <p><b>Angus Cuts</b></p>
                                    <p>3 Products</p>
                                </div>
                            </div>
                            <div class="col-md-4 col-4 my-auto">
                                <div class="box-content">
                                    <p>$135.00</p>
                                </div>
                            </div>
                        </div>
                        <hr/>
                    </div>
                    <div class="order-list">
                        <div class="row">
                            <div class="col-md-4 col-4 my-auto">
                                <div class="box-content text-center">
                                    <img src="assets/images/account/Organ-Bites-Order-List-Account.png" class="img-fluid"/>
                                </div>
                            </div>
                            <div class="col-md-4 col-4 my-auto">
                                <div class="box-content">
                                    <p><b>Organ Bites</b></p>
                                    <p>2 Products</p>
                                </div>
                            </div>
                            <div class="col-md-4 col-4 my-auto">
                                <div class="box-content">
                                    <p>$90.00</p>
                                </div>
                            </div>
                        </div>
                        <hr/>
                    </div>
                </div>
                <div class="btn-holder text-center">
                    <button class="btn btn-outline-primary text-dark">Order Confirmation</button>
                    <button class="btn btn-outline-primary text-dark btn-track-order">Track My Order</button>
                </div>
            </div>
            <div class="box-order">
                <div class="box-head">
                    <div class="row justify-content-between">
                        <div class="col-lg-4 col-md-6 col-6">
                            <p>12/08/2020</p>
                        </div>
                        <div class="col-lg-4 col-md-6 col-6">
                            <p>DHL Express Shiping</p>
                        </div>
                        <div class="col-lg-4 col-md-12 col-12">
                            <p><b>Receipt Number</b></p>
                            <p>08882924598</p>
                        </div>
                    </div>
                </div>
                <div class="box-order-desc">
                    <div class="row order-info-desk">
                        <div class="col-md-4 col-4">
                            <div class="box-desc">
                                <p>Order Number</p>
                                <p><b>#547856</b> | Print</p>
                            </div>
                        </div>
                        <div class="col-md-4 col-4">
                            <div class="box-desc">
                                <p>Status</p>
                                <p><b>Order Completed</b></p>
                            </div>
                        </div>
                        <div class="col-md-4 col-4">
                            <div class="box-desc">
                                <p>Total Price</p>
                                <p><b>$225.00</b></p>
                            </div>
                        </div>
                    </div>
                    <div class="row order-info-mobile">
                        <div class="col-md-12">
                            <div class="box-desc">
                                <div class="row">
                                    <div class="col-md-6 col-6"><p>Order Number</p></div>
                                    <div class="col-md-6 col-6"><p><b>#547856</b> | Print</p></div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6 col-6"><p>Status</p></div>
                                    <div class="col-md-6 col-6"><p><b>Order Completed</b></p></div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6 col-6"><p>Total Price</p></div>
                                    <div class="col-md-6 col-6"><p><b>$225.00</b></p></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box-list">
                    <div class="order-list">
                        <div class="row">
                            <div class="col-md-4 col-4 my-auto">
                                <div class="box-content text-center">
                                    <img src="assets/images/account/Angus-Cuts-Order-List-Account.png" class="img-fluid"/>
                                </div>
                            </div>
                            <div class="col-md-4 col-4 my-auto">
                                <div class="box-content">
                                    <p><b>Wagyu Cuts</b></p>
                                    <p>5 Products</p>
                                </div>
                            </div>
                            <div class="col-md-4 col-4 my-auto">
                                <div class="box-content">
                                    <p>$225.00</p>
                                </div>
                            </div>
                        </div>
                        <hr/>
                    </div>
                </div>
                <div class="btn-holder text-center">
                    <button class="btn btn-outline-primary text-dark">Order Confirmation</button>
                    <button class="btn btn-outline-primary text-dark btn-track-order">Track My Order</button>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="box-tracking">
    <div class="row">
        <div class="col-lg-4 col-md-12">
            <div class="box-head">
                <p class="title-form">Track<br/>My Order</p>
            </div>
        </div>
        <div class="col-lg-8 col-md-12">
            <div class="box-order">
                <div class="box-head">
                    <div class="row justify-content-between">
                        <div class="col-lg-4 col-md-6 col-6">
                            <p>01/09/2020</p>
                        </div>
                        <div class="col-lg-4 col-md-6 col-6">
                            <p>DHL Express Shiping</p>
                        </div>
                        <div class="col-lg-4 col-md-12 col-12">
                            <p><b>Receipt Number</b></p>
                            <p>0888297732</p>
                        </div>
                    </div>
                </div>
                <div class="box-order-desc">
                    <div class="row">
                        <div class="col-md-4 col-12">
                            <div class="box-desc">
                                <p>Order Number</p>
                                <p><b>#547856</b></p>
                            </div>
                        </div>
                        <div class="col-md-8 col-12">
                            <div class="box-desc">
                                <div class="box-info-customer">
                                    <p><b>Christopher Brian</b></p>
                                    <p>+8075435432</p>
                                </div>
                                <div class="box-address">
                                    <p>7/25 Adelaide St, BrisbaneCity,<br/>Queensland, Australia, 4000</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box-track-timeline">
                    <div class="content-track">
                        <p class="date-timeline">Sept 1, 2020</p>
                        <div class="container-timeline right">
                            <div class="time">
                                <p>10:27<br/><small>PM AEST</small></p>
                            </div>
                            <div class="content">
                                <h2>AUSTRALIA</h2>
                                <p>Your order has been processed and tracking will be updated soon</p>
                            </div>
                        </div>
                    </div>
                    <div class="content-track">
                        <div class="container-timeline right">
                            <div class="time">
                                <p>10:41<br/><small>PM AEST</small></p>
                            </div>
                            <div class="content">
                                <h2>AUSTRALIA</h2>
                                <p>En route to DHL eCommerce Distribution Center</p>
                            </div>
                        </div>
                    </div>
                    <div class="content-track">
                        <p class="date-timeline">Sept 3, 2020</p>
                        <div class="container-timeline right">
                            <div class="time">
                                <p>02:42<br/><small>PM SGT</small></p>
                            </div>
                            <div class="content">
                                <h2>SINGAPORE</h2>
                                <p>Arrival DHL eCommerce Facility</p>
                            </div>
                        </div>
                    </div>
                    <div class="content-track">
                        <div class="container-timeline right">
                            <div class="time">
                                <p>09:41<br/><small>PM SGT</small></p>
                            </div>
                            <div class="content">
                                <h2>SINGAPORE</h2>
                                <p>Processing Completed at Orign</p>
                            </div>
                        </div>
                    </div>
                    <div class="content-track">
                        <p class="date-timeline">Sept 5, 2020</p>
                        <div class="container-timeline right">
                            <div class="time">
                                <p>08:44<br/><small>PM JST</small></p>
                            </div>
                            <div class="content">
                                <h2>JAPAN</h2>
                                <p>Arrival at Destination Counrty</p>
                            </div>
                        </div>
                    </div>
                    <div class="content-track">
                        <div class="container-timeline right">
                            <div class="time">
                                <p>11:41<br/><small>PM JST</small></p>
                            </div>
                            <div class="content finish">
                                <h2>JAPAN</h2>
                                <p>Delivered</p>
                            </div>
                        </div>
                    </div>
                </div>
                <hr/>
                <div class="btn-holder text-center">
                    <button class="btn btn-outline-primary text-dark btn-back-order">Back to Order List</button>
                </div>
            </div>
        </div>
    </div>
</div>