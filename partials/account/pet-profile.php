<div class="box-pet-profile">
    <div class="row">
        <div class="col-lg-4 col-md-12">
            <div class="box-head">
                <p class="title-form">Pet<br/>Profile</p>
            </div>
        </div>
        <div class="col-lg-8 col-md-12">
            <div class="table-responsive">
                <table class="table">
                    <thead>
                        <tr>
                            <th>Photo</th>
                            <th>Profile</th>
                            <th>Date of Birth</th>
                            <th>Favourite</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>
                                <div class="box-photo" style="background-image: url('assets/images/account/Dog1-Pet-Profile-Account.png')">
                                    <!-- <img src="assets/images/account/Dog1-Pet-Profile-Account.png" class="img-fluid" /> -->
                                </div>
                            </td>
                            <td>
                                <b>Blacky</b><br/>Alaskan Malamute
                            </td>
                            <td>20 Jun 2019</td>
                            <td>
                                BEASTY Angus Cuts,<br/>
                                BEASTY Tendon Chew,<br/>
                                BEASTY Wagyu Cuts
                            </td>
                            <td>
                                <a href="#" class="btn-edit-pet"><i class="far fa-edit"></i></a>
                                <a href="#"><i class="far fa-trash-alt"></i></a>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="box-photo" style="background-image: url('assets/images/account/Dog2-Pet-Profile-Account.png')">
                                    <!-- <img src="assets/images/account/Dog2-Pet-Profile-Account.png" class="img-fluid" /> -->
                                </div>
                            </td>
                            <td>
                                <b>Coco</b><br/>Pug
                            </td>
                            <td>15 Mar 2017</td>
                            <td>
                                BEASTY Tendon Chew
                            </td>
                            <td>
                                <a href="#" class="btn-edit-pet"><i class="far fa-edit"></i></a>
                                <a href="#"><i class="far fa-trash-alt"></i></a>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="btn-holder">
                <button class="btn btn-outline-primary text-dark btn-new-pet">Add Profile</button>
            </div>
        </div>
    </div>
</div>

<div class="box-add-pet">
    <div class="row">
        <div class="col-lg-4 col-md-12">
            <div class="box-head">
                <p class="title-form">Pet<br/>Profile</p>
            </div>
        </div>
        <div class="col-lg-8 col-md-12">
            <form action="" class="form pet-profile">
                <div class="row">
                    <div class="col-md-8 order-lg-first order-last">
                        <div class="form-group">
                            <label>Name</label>
                            <input type="text" class="form-control" />
                        </div>
                        <div class="form-group">
                            <label>Breed</label>
                            <select class="form-control">
                                <option vlaue="0">Please select breed</option>
                                <option vlaue="1">Alaskan Malamute</option>
                                <option vlaue="2">Buldog</option>
                                <option vlaue="3">Pudel</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Date of Birth</label>
                            <input type="date" class="form-control" />
                        </div>
                        <div class="form-group">
                            <label>Favourite Product</label>
                            <select class="form-control favourite" multiple>
                                <option vlaue="1">BEASTY Angus Cuts</option>
                                <option vlaue="2">BEASTY Organ Bites</option>
                                <option vlaue="3">BEASTY Tendon Chews</option>
                                <option value="4">BEASTY Wagyu Cuts</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4 order-lg-last order-first text-center">
                        <div class="box-image-preview" style="background-image: url('assets/images/account/Empty-Pet-Profile-Account.png')">
                            <!-- <img src="assets/images/account/Empty-Pet-Profile-Account.png" class="img-fluid img-preview" /> -->
                        </div>
                        <div class="form-group">
                            <label for="upload-photo" class="btn btn-outline-primary text-dark">Add Photo</label>
                            <input type="file" name="photo" class="upload-photo" id="upload-photo" />
                        </div>
                    </div>
                </div>
            </form>
            <div class="btn-holder">
                <button class="btn btn-outline-primary text-dark btn-add-pet">Add Profile</button>
            </div>
        </div>
    </div>
</div>

<div class="box-edit-pet">
    <div class="row">
        <div class="col-lg-4 col-md-12">
            <div class="box-head">
                <p class="title-form">Edit Pet<br/>Profile</p>
            </div>
        </div>
        <div class="col-lg-8 col-md-12">
            <form action="" class="form pet-profile">
                <div class="row">
                    <div class="col-md-8 order-lg-first order-last">
                        <div class="form-group">
                            <label>Name</label>
                            <input type="text" class="form-control" value="Blacky" />
                        </div>
                        <div class="form-group">
                            <label>Breed</label>
                            <select class="form-control">
                                <option vlaue="0">Please select breed</option>
                                <option vlaue="1" selected>Alaskan Malamute</option>
                                <option vlaue="2">Buldog</option>
                                <option vlaue="3">Pudel</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Date of Birth</label>
                            <input type="date" class="form-control" value="20/06/2019" />
                        </div>
                        <div class="form-group">
                            <label for="favourite">Favourite Product</label>
                            <select class="form-control favourite" multiple>
                                <option value="">Select favourite product</option>
                                <option vlaue="1" selected>BEASTY Angus Cuts</option>
                                <option vlaue="2" selected>BEASTY Organ Bites</option>
                                <option vlaue="3">BEASTY Tendon Chews</option>
                                <option value="4">BEASTY Wagyu Cuts</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4 order-lg-last order-first text-center">
                        <div class="box-image-preview" style="background-image: url('assets/images/account/Dog1-Pet-Profile-Account.png')">
                            <!-- <img src="assets/images/account/Dog1-Pet-Profile-Account.png" class="img-fluid img-preview" /> -->
                        </div>
                        <div class="form-group">
                            <label for="edit-upload-photo" class="btn btn-outline-primary text-dark">Change Photo</label>
                            <input type="file" name="photo" class="upload-photo" id="edit-upload-photo" />
                        </div>
                    </div>
                </div>
            </form>
            <div class="btn-holder">
                <button class="btn btn-outline-primary text-dark btn-save-pet">Save Changes</button>
            </div>
        </div>
    </div>
</div>