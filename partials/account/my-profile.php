<div class="box-profile">
    <div class="row">
        <div class="col-lg-4 col-md-12">
            <div class="box-head">
                <p class="title-form">Personal<br/>Profile</p>
            </div>
        </div>
        <div class="col-lg-8 col-md-12">
            <div class="box-label">
                <div class="form-group">
                    <label>Name</label>
                    <label class="value">Christopher Brian</label>
                </div>
                <div class="form-group">
                    <label>E-mail</label>
                    <label class="value">brianchristopher@gmail.com</label>
                </div>
                <div class="form-group">
                    <label>Date Of Birth</label>
                    <label class="value">10 March 1990</label>
                </div>
                <div class="form-group">
                    <label>Phone Number</label>
                    <label class="value">+8075092299</label>
                </div>
            </div>
            <div class="btn-holder desktop">
                <button class="btn btn-outline-primary text-dark btn-edit-profile">Edit Profile</button>
                <button class="btn btn-outline-primary text-dark btn-reset-password">Reset Password</button>
            </div>
        </div>
        <div class="col-md-12">
            <div class="btn-holder text-center mobile">
                <button class="btn btn-outline-primary text-dark btn-edit-profile">Edit Profile</button>
                <button class="btn btn-outline-primary text-dark btn-reset-password">Reset Password</button>
            </div>
        </div>
    </div>
</div>

<div class="edit-profile">
    <div class="row">
        <div class="col-lg-4 col-md-12">
            <div class="box-head">
                <p class="title-form">Edit<br/>Profile</p>
            </div>
        </div>
        <div class="col-lg-8 col-md-12">
            <form action="" class="form">
                <div class="form-group">
                    <label>Name</label>
                    <input type="text" class="form-control" value="Christopher Brian" />
                </div>
                <div class="form-group">
                    <label>E-mail</label>
                    <input type="email" class="form-control" value="brianchristopher@gmail.com" />
                </div>
                <div class="form-group">
                    <label>Date Of Birth</label>
                    <input type="date" class="form-control" value="10/03/1990" />
                </div>
                <div class="form-group">
                    <label>Phone Number</label>
                    <input type="text" class="form-control" value="+8075092299" />
                </div>
            </form>
            <div class="btn-holder desktop">
                <button class="btn btn-outline-primary text-dark btn-save-profile">Save Changes</button>
            </div>
        </div>
        <div class="col-md-12">
            <div class="btn-holder text-center mobile">
                <button class="btn btn-outline-primary text-dark btn-save-profile">Save Changes</button>
            </div>
        </div>
    </div>
</div>

<div class="reset-password">
    <div class="row">
        <div class="col-lg-4 col-md-12">
            <div class="box-head">
                <p class="title-form">Reset<br/>Password</p>
            </div>
        </div>
        <div class="col-lg-8 col-md-12">
            <form action="" class="form">
                <div class="form-group">
                    <label for="current-password">Curent Password</label>
                    <input type="password" class="form-control" id="current-password" />
                </div>
                <div class="form-group">
                    <label for="new-password">New Password</label>
                    <input type="password" class="form-control" id="new-password" />
                </div>
                <div class="form-group">
                    <label for="confirm-password">Confirm Password</label>
                    <input type="password" class="form-control" id="confirm-password" />
                </div>
            </form>
            <div class="btn-holder desktop">
                <button class="btn btn-outline-primary text-dark btn-save-profile">Save Changes</button>
            </div>
        </div>
        <div class="col-md-12">
            <div class="btn-holder text-center mobile">
            <button class="btn btn-outline-primary text-dark btn-save-profile">Save Changes</button>
            </div>
        </div>
    </div>
</div>