<!-- this is top navigation -->
<nav class="navbar navbar-default fixed-top navbar-dark" role="navigation">
    <div class="container-fluid">
        <ul class="side-menu menu-mobile">
            <li>
                <a href="javascript:void(0)" class="shopping-cart-menu btn-cart">
                    <span class="count">0</span>
                    <img src="assets/images/side-menu/Shopping-Cart-Icon.svg" class="img-fluid" />
                </a>
            </li>
            <li>
                <a href="javascript:void(0)" class="account-menu" data-toggle="modal" data-target="#modalSignin">
                    <img src="assets/images/side-menu/Account-Icon.svg" class="img-fluid" />
                </a>
                <!-- If already sign in -->
                <!--
                <a href="account.php" class="account-menu" >
                    <img src="assets/images/side-menu/Account-Icon.svg" class="img-fluid" />
                </a> -->
            </li>
        </ul>
        <div class="navbar-header">
            <div class="navbar-toggle">
                <div class="menu-icon">
                    <span class="line-1"></span>
                    <span class="line-2"></span>
                </div>
            </div>
            <div class="navbar-brand navbar-brand-centered">
                <a href="index.php"><img src="assets/images/White-Logo.svg" class="img-fluid img-logo" /></a>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</nav>