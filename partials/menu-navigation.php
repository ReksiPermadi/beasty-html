<div class="box-menu">
    <div class="container-fluid">
        <div class="btn-close">
            <i class="fal fa-times"></i>
        </div>
        <ul class="menu-list">
            <li><a href="account.php"><span>Account</a></li>
            <li><a href="product.php"><span>Product</a></li>
            <li><a href="gallery.php"><span>Gallery</a></li>
            <li><a href="article.php"><span>Articles</a></li>
            <li><a href="our-story.php"><span>Our Story</a></li>
            <li><a href="contact-us.php"><span>Contact Us</a></li>
        </ul>
    </div>
</div>