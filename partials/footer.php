<!-- This is footer -->
<footer class="footer section fp-auto-height">
    <div class="container beasty-wrapper">
        <div class="row">
            <div class="col-md-6">
                <div class="box-left">
                    <div class="row">
                        <div class="col-md-4 col-4">
                            <div class="box-image-footer">
                                <img src="assets/images/footer-logo.png" class="img-fluid" />
                            </div>
                        </div>
                        <div class="col-md-8 col-8">
                            <div class="box-address">
                                <p>Level 15, Tower 1, Westfield<br/>
                                Oxford Street, Bondi Junction NSW 1501A,<br/>
                                2022, Australia, +61 (0)2 8305 0300520
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="box-right">
                    <div class="row">
                        <div class="col-md-6 col-8 order-last order-md-first">
                            <div class="box-copyright">
                                <p>&copy; 2020 Beasty<br/>
                                <a href="term-and-conditions.php">Terms and Conditions</a> | <a href="privacy-policy.php">Privacy Policy</a></p>
                            </div>
                        </div>
                        <div class="col-md-6 col-4 order-first order-md-last">
                            <ul class="social-media">
                                <!-- <li><a href="#"><i class="fab fa-facebook-f"></i></a></li> -->
                                <li><a href="https://www.instagram.com/gobeasty" target="_blank"><i class="fab fa-instagram"></i> &nbsp;<span>@gobeasty</span></a></li>
                                <!-- <li><a href="#"><i class="fab fa-twitter"></i></a></li> -->
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
