<!-- this is side navigation -->
<div class="sidebar-container">
    <div class="box-sidebar">
        <ul class="side-menu menu-desktop">
            <li>
                <a href="javascript:void(0)" class="account-menu" data-toggle="modal" data-target="#modalSignin">
                    <img src="assets/images/side-menu/Account-Icon.svg" class="img-fluid" />
                </a>
                <!-- If already sign in -->
                <!--
                <a href="account.php" class="account-menu" >
                    <img src="assets/images/side-menu/Account-Icon.svg" class="img-fluid" />
                </a> -->
            </li>
            <li>
                <a href="javascript:void(0)" class="shopping-cart-menu btn-cart">
                    <span class="count">0</span>
                    <img src="assets/images/side-menu/Shopping-Cart-Icon.svg" class="img-fluid" />
                </a>
            </li>
            <li>
                <a href="product.php" class="product-menu">
                    <img src="assets/images/side-menu/Product-Catalogue-Icon.svg" class="img-fluid" />
                </a>
            </li>
            <li>
                <a href="our-story.php" class="our-story-menu">
                    <img src="assets/images/side-menu/Our-Story-Icon.svg" class="img-fluid" />
                </a>
            </li>
        </ul>
    </div>
</div>

<!-- Modal Sign in -->
<div id="modalSignin" class="modal fade modal-signin" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal"><i class="fal fa-times"></i></button>
        <ul class="nav nav-tabs">
            <li>
                <a data-toggle="tab" href="#signin" class="active">Sign In</a>
            </li>
            <li>
                <a data-toggle="tab" href="#signup">Sign Up</a>
            </li>
        </ul>

        <div class="tab-content">
            <div id="signin" class="tab-pane fade in active show">
                <div class="box-head">
                    <p class="title-form">Welcome</p>
                    <p class="title-form white">Back !</p>
                </div>
                <div class="box-form">
                    <form action="" class="form">
                        <div class="form-group">
                            <label>Email</label>
                            <input type="email" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>Password</label>
                            <input type="password" class="form-control">
                        </div>
                        <div class="box-forgot">
                            <div class="checkbox">
                                <label><input type="checkbox" value="">Remember Me</label>
                            </div>
                            <a href="javascript:void(0)" class="forgot-password">Forgot Password?</a>
                        </div>
                        <div class="text-center btn-holder">
                            <button type="submit" class="btn btn-outline-secondary">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
            <div id="signup" class="tab-pane fade">
                <div class="box-head create-account">
                    <p class="title-form">Create Your</p>
                    <p class="title-form white">Account</p>
                </div>

                <div class="box-form first-step">
                    <form action="" class="form">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="first-name">First Name</label>
                                    <input type="text" class="form-control" id="first-name">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="last-name">Last Name</label>
                                    <input type="text" class="form-control" id="last-name">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Email</label>
                            <input type="email" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>Password</label>
                            <input type="password" class="form-control">
                        </div>
                        <div class="text-center btn-holder">
                            <button type="button" class="btn btn-outline-secondary next-signup">Next</button>
                        </div>
                    </form>
                </div>

                <div class="box-form next-step">
                    <form action="" class="form">
                        <div class="form-group">
                            <label for="pet-name">Pet Name</label>
                            <input type="text" class="form-control" id="pet-name">
                        </div>
                        <div class="form-group">
                            <label for="breed">Breed</label>
                            <select class="form-control" id="breed">
                                <option>Buldog</option>
                                <option>Pudel</option>
                                <option>Beagle</option>
                                <option>Dachshund</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="pet-birth">Pet Date of Birth</label>
                            <input type="date" class="form-control" id="pet-birth">
                        </div>
                        <div class="text-center btn-holder">
                            <a href="javascript:void(0)" class="btn-back back-signup">
                                <i class="fal fa-chevron-left"></i>Back
                            </a>
                            <button type="button" class="btn btn-outline-secondary btn-signup">Sign Up</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
      </div>
    </div>

  </div>
</div>

<!-- Modal Success -->
<div id="modalSuccess" class="modal fade modal-signin modal-success" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal"><i class="fal fa-times"></i></button>
        <div class="box-head small">
            <p class="title-form">CONGRATULATIONS!</p>
            <p class="title-form">YOUR ACCOUNT HAS BEEN</p>
        </div>
        <div class="box-head">
            <p class="title-form">Successfully</p>
            <p class="title-form orange">Created</p>
        </div>
        <div class="box-image">
            <img src="assets/images/sign-in/Success-Sign-Up.png" class="img-fluid" />
        </div>
      </div>
    </div>

  </div>
</div>

<!-- Modal Forgot Password -->
<div id="modalForgotPassword" class="modal fade modal-signin modal-success modal-forgot-password" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal"><i class="fal fa-times"></i></button>
        <div class="box-head">
            <p class="title-form">Forgot Your</p>
            <p class="title-form orange">Password?</p>
        </div>
        <div class="box-head small">
            <p class="title-form">Don't Worry<br/>We Will Send You an Email<br/>to Reset Your Password.</p>
        </div>
        <div class="box-form ">
            <form action="" class="form">
                <div class="form-group">
                    <label>Email</label>
                    <input type="text" class="form-control">
                </div>
                <div class="text-center btn-holder">
                    <button type="submit" class="btn btn-outline-primary text-dark">Submit</button>
                </div>
            </form>
        </div>
      </div>
    </div>

  </div>
</div>

<!-- Box Cart -->
<div class="box-cart">
    <div class="box-head">
        <button type="button" class="close btn-close-cart"><i class="fal fa-times"></i></button>
        <h3 class="title-cart">Shopping Cart</h3>
    </div>
    <div class="box-list-cart">
        <div class="list-cart">
            <div class="box-image">
                <img src="assets/images/cart/Angus-Cuts-Cart.png" class="img-fluid" />
            </div>
            <div class="box-desc">
                <button type="button" class="close remove-product"><i class="fal fa-times"></i></button>
                <h4>Angus Cuts</h4>
                <p class="price-product"><b>$<span>45</span></b> | 3 oz (85 g)</p>
                <div class="box-form form-inline box-quantity">
                    <button class="btn btn-oval btn-min-calculate"><i class="fas fa-minus"></i></button>
                    <input type="number" class="form-control number quantity" value="1" />
                    <button class="btn btn-oval btn-plus-calculate"><i class="fas fa-plus"></i></button>
                    <div class="text-right">
                        <p class="total-price">$<span>45.00</span></p>
                    </div>
                </div>
            </div>
        </div>
        <div class="list-cart">
            <div class="box-image">
                <img src="assets/images/cart/Organ-Bites-Cart.png" class="img-fluid" />
            </div>
            <div class="box-desc">
                <button type="button" class="close remove-product"><i class="fal fa-times"></i></button>
                <h4>Organ Bites</h4>
                <p class="price-product"><b>$<span>45</span></b> | 3 oz (85 g)</p>
                <div class="box-form form-inline box-quantity">
                    <button class="btn btn-oval btn-min-calculate"><i class="fas fa-minus"></i></button>
                    <input type="number" class="form-control number quantity" value="2" />
                    <button class="btn btn-oval btn-plus-calculate"><i class="fas fa-plus"></i></button>
                    <div class="text-right">
                        <p class="total-price">$<span>90.00</span></p>
                    </div>
                </div>
            </div>
        </div>
        <div class="list-cart">
            <div class="box-image">
                <img src="assets/images/cart/Tendon-Chews-Cart.png" class="img-fluid" />
            </div>
            <div class="box-desc">
                <button type="button" class="close remove-product"><i class="fal fa-times"></i></button>
                <h4>Tendon Chews</h4>
                <p class="price-product"><b>$<span>45</span></b> | 3 oz (85 g)</p>
                <div class="box-form form-inline box-quantity">
                    <button class="btn btn-oval btn-min-calculate"><i class="fas fa-minus"></i></button>
                    <input type="number" class="form-control number quantity" value="2" />
                    <button class="btn btn-oval btn-plus-calculate"><i class="fas fa-plus"></i></button>
                    <div class="text-right">
                        <p class="total-price">$<span>90.00</span></p>
                    </div>
                </div>
            </div>
        </div>
        <div class="list-cart">
            <div class="box-image">
                <img src="assets/images/cart/Angus-Cuts-Cart.png" class="img-fluid" />
            </div>
            <div class="box-desc">
                <button type="button" class="close remove-product"><i class="fal fa-times"></i></button>
                <h4>Wagyu Cuts</h4>
                <p class="price-product"><b>$<span>45</span></b> | 3 oz (85 g)</p>
                <div class="box-form form-inline box-quantity">
                    <button class="btn btn-oval btn-min-calculate"><i class="fas fa-minus"></i></button>
                    <input type="number" class="form-control number quantity" value="1" />
                    <button class="btn btn-oval btn-plus-calculate"><i class="fas fa-plus"></i></button>
                    <div class="text-right">
                        <p class="total-price">$<span>45.00</span></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="btn-holder text-center">
        <p class="total-checkout">Total: $225.00</p>
        <a href="checkout.php" class="btn btn-outline-primary text-dark">checkout</a>
    </div>
</div>
<div class="modal-backdrop fade"></div>