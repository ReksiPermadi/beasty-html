<!DOCTYPE html>
<html>
	<head>
		<title>Beasty - Order Confirmation</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, user-scalable=no">

		<?php include("partials/head.php") ?>
		
	</head>
	<body>
		<?php include("partials/top-navigation-dark.php") ?>
		<?php include("partials/side-navigation.php") ?>
		<?php include("partials/menu-navigation.php") ?>

		<div class="main-content">
			<section class="shipping-list">
                <div class="container beasty-wrapper">
                    <div class="box-head">
                        <h3 class="title">Order Confirmation</h3>
                    </div>
                    <div class="box-info-order">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="box-content">
                                    <h3 class="title">order number<br/>#547856</h3>
                                    <p class="desc">We sent a confirmation email to brianchristopher@gmail.com. Below you will find all you information about your order.</p>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="box-content">
                                    <h3 class="title">Shipping Address</h3>
                                    <p class="subtitle">Christopher Brian</p>
                                    <p class="desc">+8075435432<br/>brianchristopher@gmail.com<br/> 7/25 Adelaide St, BrisbaneCity, Queensland, Australia, 4000</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section class="shipping-progress">
                <div class="container beasty-wrapper">
                    <ul class="box-progress">
                        <li class="active">Shipping Details</li>
                        <li class="active">Payment Details</li>
                        <li class="active">Order Confirmation</li>
                    </ul>
                    <div class="line"></div>
                </div>
            </section>

            <section class="product-info">
                <div class="container beasty-wrapper">
                    <h4 class="title-checkout">Product Summary</h4>
                    <div class="box-order">
                        <div class="box-head">
                            <div class="row justify-content-between">
                                <div class="col-md-4 col-6">
                                    <p>01/09/2020</p>
                                </div>
                                <div class="col-md-4 col-6">
                                    <p>DHL Express Shiping</p>
                                </div>
                            </div>
                        </div>
                        <div class="box-order-desc">
                            <div class="row">
                                <div class="col-md-4 col-6">
                                    <div class="box-desc">
                                        <p>Order Number</p>
                                        <p><b>#547856</b> | Print</p>
                                    </div>
                                </div>
                                <div class="col-md-4 col-6">
                                    <div class="box-desc">
                                        <p>Status</p>
                                        <p><b>In-Delivery</b></p>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="box-desc">
                                        <p>Total Price</p>
                                        <p><b>$225.00</b></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="box-list">
                            <div class="order-list">
                                <div class="row">
                                    <div class="col-md-4 col-4 my-auto">
                                        <div class="box-content text-center">
                                            <img src="assets/images/account/Angus-Cuts-Order-List-Account.png" class="img-fluid"/>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-4 my-auto">
                                        <div class="box-content">
                                            <p><b>Angus Cuts</b></p>
                                            <p><b>$45</b></p>
                                            <p>3 Products</p>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-4 my-auto">
                                        <div class="box-content">
                                            <p>$135.00</p>
                                        </div>
                                    </div>
                                </div>
                                <hr/>
                            </div>
                            <div class="order-list">
                                <div class="row">
                                    <div class="col-md-4 col-4 my-auto">
                                        <div class="box-content text-center">
                                            <img src="assets/images/account/Organ-Bites-Order-List-Account.png" class="img-fluid"/>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-4 my-auto">
                                        <div class="box-content">
                                            <p><b>Organ Bites</b></p>
                                            <p><b>$45</b></p>
                                            <p>2 Products</p>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-4 my-auto">
                                        <div class="box-content">
                                            <p>$90.00</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 mt-3">
                            <h4 class="title-checkout">Payment Method</h4>
                            <div class="box-payment">
                                <div class="box-image">
                                    <img src="assets/images/payment/Visa-Logo-Payment.png" class="img-fluid" />
                                </div>
                                <div class="box-text">
                                    <p>Card Ending 4554</p>
                                    <small>Credit Card via Stripe</small>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 mt-3">
                            <h4 class="title-checkout">Total</h4>
                            <div class="list-count">
                                <div class="row">
                                    <div class="col-md-6 col-6">
                                        <p class="label">Items Subtotal</p>
                                    </div>
                                    <div class="col-md-6 col-6 text-right">
                                        <p>$ 225.00</p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6 col-6">
                                        <p class="label">Shipping</p>
                                    </div>
                                    <div class="col-md-6 col-6 text-right">
                                        <p>$ 0.00</p>
                                    </div>
                                </div>
                            </div>
                            <div class="list-count total">
                                <div class="row">
                                    <div class="col-md-6 col-6">
                                        <p class="label"><b>Total</b></p>
                                    </div>
                                    <div class="col-md-6 col-6 text-right">
                                        <p><b>$ 225.00</b></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
			
            <?php include("partials/footer.php") ?>
		</div>

        <?php include("partials/script.php") ?>
	</body>
</html>