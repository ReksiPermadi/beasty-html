<!DOCTYPE html>
<html>
	<head>
		<title>Beasty - Our Story</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, user-scalable=no">

		<?php include("partials/head.php") ?>
		
	</head>
	<body>
		<?php include("partials/top-navigation.php") ?>
		<?php include("partials/side-navigation.php") ?>
		<?php include("partials/menu-navigation.php") ?>

		<div class="main-content" id="our-story">
            <section class="section section-banner-our-story" style="background-image: url(assets/images/our-story/Brush-Background-Image-Story.png), url(assets/images/bg-dark.jpg);">
                <div class="container beasty-wrapper">
                    <div class="row">
                        <div class="col-lg-6 col-md-12">
                            <div class="box-content">
                                <p class="small-title">Our Story</p>
                                <h3 class="top-title">Made From</h3>
                                <h1 class="title">The Best</h1>
                                <h4 class="bottom-title">Mother Nature Offers</h4>
                                <p class="description">Beasty by Crown State Pastoral was created by a
                                family of farmers with a love for pets. Located in
                                New South Wales Australia, we have been raising
                                award-winning cattle for the past 10 years. </p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section class="section section-ingridient-our-story">
                <div class="container beasty-wrapper">
                    <div class="row justify-content-end">
                        <div class="col-lg-6 col-md-12">
                            <div class="box-content">
                                <h1 class="title">We Own The Land</h1>
                                <h4 class="subtitle">AND RAISED the cattle</h4>
                                <div class="box-desc">
                                    <p class="description">With love and dedication, we raised the best
                                    cattle without GMO's and are 100% hormone-free.
                                    We feel our customers should know how their
                                    food was crafted. </p>
                                    <p class="description">For a decade, we have fed the world delicious and
                                    natural beef. Now, we feed your best friend too.
                                    As the age-old saying goes, you are what you eat. </p>
                                    <p class="description"><b>You eat clean, shouldn't you feed your
                                    companion the same? </b></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section class="section section-process">
                <div class="container beasty-wrapper">
                    <div class="box-content">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="list-content">
                                    <p class="title mobile">Farm</p>
                                    <img src="assets/images/our-story/Farm-Story.png" class="img-fluid" />
                                    <p class="title desktop">Farm</p>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="list-content">
                                    <p class="title small">To</p>
                                    <img src="assets/images/our-story/Bowl-Story.png" class="img-fluid" />
                                    <p class="title">Bowl</p>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="box-text-bottom text-center">
                                    <p class="text-dark">To</p>
                                    <p>TRACEABILITY FROM START TO FINISH</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section class="section section-slogan" style="background-image: url(assets/images/our-story/Dog-Story.png), url(assets/images/our-story/Floating-Beef-Story.png);">
                <div class="container beasty-wrapper">
                    <div class="row">
                        <div class="col-lg-6 col-md-12">
                            <div class="box-slogan">
                                <div class="list-slogan">
                                    <h3 class="title">Health</h3>
                                    <p class="desc">Highly nutritious while being <br/> GMO and HGP free</p>
                                </div>
                                <div class="list-slogan">
                                    <h3 class="title">Tasty</h3>
                                    <p class="desc">Exceptional taste <br/> for your pets.</p>
                                </div>
                                <div class="list-slogan">
                                    <h3 class="title">Flexible</h3>
                                    <p class="desc">For both dogs and cats (with <br/> exception of the tendon chews). </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section class="section section-made" style="background-image: url(assets/images/our-story/Australia-Farm-Story.png), url(assets/images/bg-dark.jpg);">
                <div class="container beasty-wrapper">
                    <div class="row justify-content-end">
                        <div class="col-lg-5 col-md-12">
                            <div class="box-content">
                                <h1 class="percent">100%</h1>
                                <h2 class="title">australian <span>made</span></h4>
                                <p class="description">Supports Australian farmers, producers, and small business owners.</p>
                                <div class="clear"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            
			
			<?php include("partials/footer.php") ?>
		</div>

        <?php include("partials/script.php") ?>
        <script>
			$(document).ready(function(){
				var ourStory = $('#our-story').fullpage({
                    navigation: true,
                    navigationPosition: 'right',
                    bigSectionsDestination: 'top',
                    afterLoad: function(index, nextIndex, direction){
                        if(nextIndex == 1){
                            $('.menu-icon').addClass('color-white');
                            $('.img-logo').attr('src','assets/images/White-Logo.svg');
                            $('.menu-mobile .account-menu img').attr('src','assets/images/side-menu/Account-Icon.svg');
                            $('.menu-mobile .shopping-cart-menu img').attr('src','assets/images/side-menu/Shopping-Cart-Icon.svg');
                            $('.menu-mobile .product-menu img').attr('src','assets/images/side-menu/Product-Catalogue-Icon.svg');
                            $('.menu-mobile .our-story-menu img').attr('src','assets/images/side-menu/Our-Story-Icon.svg'); 
                        }else{
                            $('.menu-icon').removeClass('color-white');
                            $('.img-logo').attr('src','assets/images/Beasty-Logo.svg');
                        }
                    },
                    onLeave: function(index, nextIndex, direction){        
                        if(nextIndex == 1){
                            $('.top-nav').removeClass('with-bg');
                            $('.img-logo').attr('src','assets/images/White-Logo.svg');
                            $('.menu-mobile .account-menu img').attr('src','assets/images/side-menu/Account-Icon.svg');
                            $('.menu-mobile .shopping-cart-menu img').attr('src','assets/images/side-menu/Shopping-Cart-Icon.svg');
                            $('.menu-mobile .product-menu img').attr('src','assets/images/side-menu/Product-Catalogue-Icon.svg');
                            $('.menu-mobile .our-story-menu img').attr('src','assets/images/side-menu/Our-Story-Icon.svg'); 
                        }else{
                            $('.top-nav').addClass('with-bg');
                            $('.img-logo').attr('src','assets/images/Beasty-Logo.svg'); 
                            $('.menu-mobile .account-menu img').attr('src','assets/images/side-menu/Account-Icon-Dark.svg');
                            $('.menu-mobile .shopping-cart-menu img').attr('src','assets/images/side-menu/Shopping-Cart-Icon-Dark.svg');
                            $('.menu-mobile .product-menu img').attr('src','assets/images/side-menu/Product-Catalogue-Icon-Dark.svg');
                            $('.menu-mobile .our-story-menu img').attr('src','assets/images/side-menu/Our-Story-Icon.svg-Dark');  
                        }

                        if(nextIndex == 3) {
                            $('#fp-nav').addClass('dark');
                            $('.menu-desktop .account-menu img').attr('src','assets/images/side-menu/Account-Icon-Dark.svg');
                            $('.menu-desktop .shopping-cart-menu img').attr('src','assets/images/side-menu/Shopping-Cart-Icon-Dark.svg');
                            $('.menu-desktop .product-menu img').attr('src','assets/images/side-menu/Product-Catalogue-Icon-Dark.svg');
                            $('.menu-desktop .our-story-menu img').attr('src','assets/images/side-menu/Our-Story-Icon-Dark.svg'); 
                        } else {
                            $('#fp-nav').removeClass('dark');
                            $('.menu-desktop .account-menu img').attr('src','assets/images/side-menu/Account-Icon.svg');
                            $('.menu-desktop .shopping-cart-menu img').attr('src','assets/images/side-menu/Shopping-Cart-Icon.svg');
                            $('.menu-desktop .product-menu img').attr('src','assets/images/side-menu/Product-Catalogue-Icon.svg');
                            $('.menu-desktop .our-story-menu img').attr('src','assets/images/side-menu/Our-Story-Icon.svg');
                        }
                    }
                });
			});
		</script>
	</body>
</html>