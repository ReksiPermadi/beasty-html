<!DOCTYPE html>
<html>
	<head>
		<title>Beasty - Article Detail</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, user-scalable=no">

		<?php include("partials/head.php") ?>
		
	</head>
	<body>
		<?php include("partials/top-navigation.php") ?>
		<?php include("partials/side-navigation.php") ?>
		<?php include("partials/menu-navigation.php") ?>

		<div class="main-content">
            <section class="section-banner-news-detail">
                <div class="container beasty-wrapper box-news-detail">
                    <div class="box-head">
                        <p class="date-news">31 Aug 2020</p>
                        <h1 class="title-news">Preparing your dog for a change in routine.</h1>
                    </div>                    
                    <div class="content-carousel">
                        <div class="thumbnail-news owl-carousel">
                            <div> <img src="assets/images/article/details/Full-Photo1.jpg"> </div>
                            <div> <img src="assets/images/article/details/Full-Photo2.jpg"> </div>
                            <div> <img src="assets/images/article/details/Full-Photo3.jpg"> </div>
                        </div>
                    </div>
                </div>
            </section>
            <section class="section-content-news">
                <div class="container box-content">
                    <div class="content">
                        <h4 class="title-news">Ilicitam, explaceatur, quo corae dolum explandio. Nam in cus vereceres audit essequa tibusda quam qui ab imus, ullestis et quo idelicipsa suntur?</h4>
                        <p>Nem hiciate nimi, non elit, consed quis commos voluptium vel ipietus, occus, to con
                            re voluptate nusciunt. Totatures ut is recum accus min cuptatem que sant fugia ne
                            et labo. Magnis nis acitis doluptia corepedit ilisin consectum ut rectempero verspiet
                            omnia perisimus aut aut eat arumquatur aut fugitas dit volenet a sum sit, officid
                            ebitecaborem volesciat. Equam, sit eturem quaepra si quam esequiassunt exerrum
                            ide molores es sae. Nequis simpori occaeped molore, voluptaquis de iur sit, qui
                            serum nitas a voluptatia consequiaes doluptatum laute earum eum acienis volor
                            mollore platem ipideri quas quiatem et lam illuptas dipsant</p>

                        <p>Nem hiciate nimi, non elit, consed quis commos voluptium vel ipietus, occus, to con
                            re voluptate nusciunt. Totatures ut is recum accus min cuptatem que sant fugia ne
                            et labo. Magnis nis acitis doluptia corepedit ilisin consectum ut rectempero verspiet
                            omnia perisimus aut aut eat arumquatur aut fugitas dit volenet a sum sit, officid
                            ebitecaborem volesciat. Equam, sit eturem quaepra si quam esequiassunt exerrum
                            ide molores es sae. Nequis simpori occaeped molore, voluptaquis de iur sit, qui
                            serum nitas a voluptatia consequiaes doluptatum laute earum eum acienis volor
                            mollore platem ipideri quas quiatem et lam illuptas dipsant.</p>

                        <p>Nem hiciate nimi, non elit, consed quis commos voluptium vel ipietus, occus, to con
                            re voluptate nusciunt. Totatures ut is recum accus min cuptatem que sant fugia ne
                            et labo. Magnis nis acitis doluptia corepedit ilisin consectum ut rectempero verspiet
                            omnia perisimus aut aut eat arumquatur aut fugitas dit volenet a sum sit, officid
                            ebitecaborem volesciat. Equam, sit eturem quaepra si quam esequiassunt exerrum
                            ide molores es sae. Nequis simpori occaeped molore, voluptaquis de iur sit, qui
                            serum nitas a voluptatia consequiaes doluptatum laute earum eum acienis volor
                            mollore platem ipideri quas quiatem et lam illuptas dipsant.</p>
                    </div>
                    <ul class="box-share">
                        <li><span>Share</span></li>
                        <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                        <li><a href="#"><i class="fab fa-instagram"></i></a></li>
                        <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                    </ul>
                </div>
            </section>

            <section class="section section-latest-news detail-news">
				<div class="container beasty-wrapper">
					<div class="box-head text-center">
						<h3 class="title-small">Latest News</h3>
					</div>
					<div class="latest-news owl-carousel">
						<div class="item-news" style="background: url(assets/images/homepage/news/Latest-News-1.jpg); background-size: cover;">
                            <div class="box-image">
                                <img src="assets/images/homepage/news/Latest-News-1.jpg" class="img-fluid" />
                            </div>
                            <div class="content-news">
								<h5 class="title-news">Preparing your dog for<br/> a change  in routine</h5>
								<a href="article-detail.php" class="link-more">read more</a>
							</div>
						</div>
						<div class="item-news" style="background: url(assets/images/homepage/news/Latest-News-2.jpg); background-size: cover;">
                            <div class="box-image">
                                <img src="assets/images/homepage/news/Latest-News-2.jpg" class="img-fluid" />
                            </div>
                            <div class="content-news">
								<h5 class="title-news">A healthy dog is<br/> an active dog</h5>
								<a href="article-detail.php" class="link-more">read more</a>
							</div>
						</div>
						<div class="item-news" style="background: url(assets/images/homepage/news/Latest-News-3.jpg); background-size: cover;">
                            <div class="box-image">
                                <img src="assets/images/homepage/news/Latest-News-3.jpg" class="img-fluid" />
                            </div>
                            <div class="content-news">
								<h5 class="title-news">Heeping your best friend<br/> happy with beasty</h5>
								<a href="article-detail.php" class="link-more">read more</a>
							</div>
						</div>
						<div class="item-news" style="background: url(assets/images/homepage/news/Latest-News-1.jpg); background-size: cover;">
                            <div class="box-image">
                                <img src="assets/images/homepage/news/Latest-News-1.jpg" class="img-fluid" />
                            </div>
                            <div class="content-news">
								<h5 class="title-news">Preparing your dog for a change  in routine</h5>
								<a href="article-detail.php" class="link-more">read more</a>
							</div>
						</div>
						<div class="item-news" style="background: url(assets/images/homepage/news/Latest-News-2.jpg); background-size: cover;">
                            <div class="box-image">
                                <img src="assets/images/homepage/news/Latest-News-2.jpg" class="img-fluid" />
                            </div>
                            <div class="content-news">
								<h5 class="title-news">A healthy dog is an active dog</h5>
								<a href="article-detail.php" class="link-more">read more</a>
							</div>
						</div>
						<div class="item-news" style="background: url(assets/images/homepage/news/Latest-News-3.jpg); background-size: cover;">
                            <div class="box-image">
                                <img src="assets/images/homepage/news/Latest-News-3.jpg" class="img-fluid" />
                            </div>
                            <div class="content-news">
								<h5 class="title-news">Heeping your best friend happy with beasty</h5>
								<a href="article-detail.php" class="link-more">read more</a>
							</div>
						</div>
					</div>
					<div class="text-center">
						<a href="article.php" class="btn btn-outline-primary">Show More</a>
					</div>
				</div>
			</section>
			
			<?php include("partials/footer.php") ?>
		</div>

        <?php include("partials/script.php") ?>
        <script>
            $(document).ready(function(){
                $(".thumbnail-news").owlCarousel({    
                    loop:true,
                    items:1,
                    nav: true,
                    margin:0,
                    stagePadding: 0,
                    autoplay:false  
                });

                dotcount = 1;

                jQuery('.owl-dot').each(function() {
                    jQuery( this ).addClass( 'dotnumber' + dotcount);
                    jQuery( this ).attr('data-info', dotcount);
                    dotcount=dotcount+1;
                });

                slidecount = 1;

                jQuery('.owl-item').not('.cloned').each(function() {
                    jQuery( this ).addClass( 'slidenumber' + slidecount);
                    slidecount=slidecount+1;
                });

                jQuery('.owl-dot').each(function() {	
                    grab = jQuery(this).data('info');		
                    slidegrab = jQuery('.slidenumber'+ grab +' img').attr('src');
                    jQuery(this).css("background-image", "url("+slidegrab+")");  	
                });

                amount = $('.owl-dot').length;
                gotowidth = 100/amount;			
                jQuery('.owl-dot').css("height", gotowidth+"%");
                
                $('.account-menu img').attr('src','assets/images/side-menu/Account-Icon-Dark.svg');
                $('.shopping-cart-menu img').attr('src','assets/images/side-menu/Shopping-Cart-Icon-Dark.svg');
                $('.product-menu img').attr('src','assets/images/side-menu/Product-Catalogue-Icon-Dark.svg');
                $('.our-story-menu img').attr('src','assets/images/side-menu/Our-Story-Icon-Dark.svg');

            });

            $(window).scroll(function(){
                var banner = $('.section-banner-news-detail').height();
                var top = $(window).scrollTop() + $('.box-sidebar').height();
                if (top > banner) {
                    $('.account-menu img').attr('src','assets/images/side-menu/Account-Icon.svg');
                    $('.shopping-cart-menu img').attr('src','assets/images/side-menu/Shopping-Cart-Icon.svg');
                    $('.product-menu img').attr('src','assets/images/side-menu/Product-Catalogue-Icon.svg');
                    $('.our-story-menu img').attr('src','assets/images/side-menu/Our-Story-Icon.svg');
                } else {
                    $('.account-menu img').attr('src','assets/images/side-menu/Account-Icon-Dark.svg');
                    $('.shopping-cart-menu img').attr('src','assets/images/side-menu/Shopping-Cart-Icon-Dark.svg');
                    $('.product-menu img').attr('src','assets/images/side-menu/Product-Catalogue-Icon-Dark.svg');
                    $('.our-story-menu img').attr('src','assets/images/side-menu/Our-Story-Icon-Dark.svg');
                }
            });
        </script>
	</body>
</html>