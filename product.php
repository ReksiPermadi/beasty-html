<!DOCTYPE html>
<html>
	<head>
		<title>Beasty - Product</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, user-scalable=no">

		<?php include("partials/head.php") ?>
		
	</head>
	<body>
		<?php include("partials/top-navigation.php") ?>
		<?php include("partials/side-navigation.php") ?>
		<?php include("partials/menu-navigation.php") ?>

		<div class="main-content" id="product">
			<section class="section section-banner-product" style="background-image: url(assets/images/product/Farm--Product-Catalogue.png), url(assets/images/product/Yellow-Background-Product-Catalogue.jpg);">
				<div class="container beasty-wrapper">
					<div class="box-content">
						<div class="content-banner">
							<div class="small-title">
								<h3>Our Natural And</h3>
							</div>
							<div class="big-title">
								<h1>Well Made</h1>
							</div>
							<div class="big-title text-dark">
								<h1>Products</h1>
							</div>
						</div>
						<!-- <div class="img-banner">
							<img src="assets/images/product/Farm--Product-Catalogue.png" class="img-fluid"/>
						</div> -->
					</div>
				</div>
			</section>

			<section class="section section-product-list">
				<div class="container beasty-wrapper">
					<div class="list-product">
						<div class="row">
							<div class="col-lg-3 col-md-6 col-6 product-item">
								<div class="box-image">
									<img src="assets/images/product/B_AngusCuts_hero.png" class="img-fluid" />
								</div>
								<h4 class="title-product">Angus Cuts</h4>
								<p class="desc-product">100% Grass-Fed Beef</p>
								<p class="price-product"><b>$45</b> | 85 g</p>
								<a href="product-detail-angus-cuts.php" class="btn btn-outline-primary text-dark">View Product</a>
							</div>
							<div class="col-lg-3 col-md-6 col-6 product-item">
								<div class="box-image">
									<img src="assets/images/product/B_OrganBites_hero.png" class="img-fluid" />
								</div>
								<h4 class="title-product">Organ Bites</h4>
								<p class="desc-product">100% Organ Mix</p>
								<p class="price-product"><b>$45</b> | 85 g</p>
								<a href="product-detail-organ-bites.php" class="btn btn-outline-primary text-dark">View Product</a>
							</div>
							<div class="col-lg-3 col-md-6 col-6 product-item">
								<div class="box-image">
									<img src="assets/images/product/B_TendonChews_hero.png" class="img-fluid" />
								</div>
								<h4 class="title-product">Tendon Chews</h4>
								<p class="desc-product">100% Beef Tendon</p>
								<p class="price-product"><b>$45</b> | 85 g</p>
								<a href="product-detail-tendon-chews.php" class="btn btn-outline-primary text-dark">View Product</a>
							</div>
							<div class="col-lg-3 col-md-6 col-6 product-item">
								<div class="box-image">
									<img src="assets/images/product/B_Wagyu_hero.png" class="img-fluid" />
								</div>
								<h4 class="title-product">Wagyu Cuts</h4>
								<p class="desc-product">100% Wagyu Beef</p>
								<p class="price-product"><b>$45</b> | 85 g</p>
								<a href="product-detail-wagyu-cuts.php" class="btn btn-outline-primary text-dark">View Product</a>
							</div>
						</div>
					</div>
				</div>
			</section>

			<section class="section section-quotes-product">
				<div class="container beasty-wrapper">
					<div class="row">
						<div class="col-lg-6 col-md-12">
							<div class="box-side-image">
								<!-- <img src="assets/images/product/Organ.png" class="img-fluid food-item organ" />
								<img src="assets/images/product/Beef.png" class="img-fluid food-item beef" />
								<img src="assets/images/product/Wagyu.png" class="img-fluid food-item wagyu" />
								<img src="assets/images/product/Tendon.png" class="img-fluid food-item tendon" /> -->
								<img src="assets/images/product/Yellow-Brush-with-Products-Product-Catalogue.png" class="img-fluid" />
							</div>
						</div>
						<div class="col-lg-6 col-md-12">
							<div class="box-content-quotes">
								<h3 class="title-quotes text-white">Raw Treats</h3>
								<h3 class="title-quotes">For Dog</h3>
								<p class="hastag-quotes text-white">#GoBeasty</p>
							</div>
						</div>
					</div>
				</div>
			</section>
			
			<?php include("partials/footer.php") ?>
		</div>

		<?php include("partials/script.php") ?>
		<script>
			$(document).ready(function(){
				var product = $('#product').fullpage({
					navigation: true,
					navigationPosition: 'right',
					bigSectionsDestination: 'top',
					afterLoad: function(index, nextIndex, direction){
						if(nextIndex == 1){
							$('.menu-desktop .account-menu img').attr('src','assets/images/side-menu/Account-Icon-Dark.svg');
							$('.menu-desktop .shopping-cart-menu img').attr('src','assets/images/side-menu/Shopping-Cart-Icon-Dark.svg');
							$('.menu-desktop .product-menu img').attr('src','assets/images/side-menu/Product-Catalogue-Icon-Dark.svg');
							$('.menu-desktop .our-story-menu img').attr('src','assets/images/side-menu/Our-Story-Icon-Dark.svg'); 
						}else{
							$('.menu-desktop .account-menu img').attr('src','assets/images/side-menu/Account-Icon.svg');
							$('.menu-desktop .shopping-cart-menu img').attr('src','assets/images/side-menu/Shopping-Cart-Icon.svg');
							$('.menu-desktop .product-menu img').attr('src','assets/images/side-menu/Product-Catalogue-Icon.svg');
							$('.menu-desktop .our-story-menu img').attr('src','assets/images/side-menu/Our-Story-Icon.svg'); 
						}
					},
					onLeave: function(index, nextIndex, direction){     
						if(nextIndex == 1){
							$('.top-nav').removeClass('with-bg');
							$('.menu-desktop .account-menu img').attr('src','assets/images/side-menu/Account-Icon-Dark.svg');
							$('.menu-desktop .shopping-cart-menu img').attr('src','assets/images/side-menu/Shopping-Cart-Icon-Dark.svg');
							$('.menu-desktop .product-menu img').attr('src','assets/images/side-menu/Product-Catalogue-Icon-Dark.svg');
							$('.menu-desktop .our-story-menu img').attr('src','assets/images/side-menu/Our-Story-Icon-Dark.svg'); 
						}else{
							$('.top-nav').addClass('with-bg');  
							$('.menu-desktop .account-menu img').attr('src','assets/images/side-menu/Account-Icon.svg');
							$('.menu-desktop .shopping-cart-menu img').attr('src','assets/images/side-menu/Shopping-Cart-Icon.svg');
							$('.menu-desktop .product-menu img').attr('src','assets/images/side-menu/Product-Catalogue-Icon.svg');
							$('.menu-desktop .our-story-menu img').attr('src','assets/images/side-menu/Our-Story-Icon.svg');
						}
					}
				});
			});
		</script>
	</body>
</html>