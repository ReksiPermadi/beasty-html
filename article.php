<!DOCTYPE html>
<html>
	<head>
		<title>Beasty - Article</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, user-scalable=no">

		<?php include("partials/head.php") ?>
		
	</head>
	<body>
		<?php include("partials/top-navigation.php") ?>
		<?php include("partials/side-navigation.php") ?>
		<?php include("partials/menu-navigation.php") ?>

		<div class="main-content">
			<section class="section-banner-latest-news" style="background-image: url(assets/images/article/Brush-Backgrounnd-Image-Article.png)">
                <div class="container beasty-wrapper">
                    <div class="small-title mobile">
                        <h3>Latest Article</h3>
                    </div>
                </div>
                <div class="box-image">
                    <img src="assets/images/article/Brush-Backgrounnd-Image-Article.png" class="img-fluid" />
                </div>
                <div class="container beasty-wrapper">
					<div class="row">
                        <div class="col-lg-6 offset-lg-6 col-md-12">
                            <div class="content-banner">
                                <div class="small-title desktop">
                                    <h3>Latest Article</h3>
                                </div>
                                <div class="big-title">
                                    <h1>Preparing your dog for a change in routine.</h1>
                                </div>
                                <p class="description">Right now your dog is enjoying this extra time with you at home, but they may struggle to adjust once ... </p>
                                <a href="article-detail.php" class="btn btn-outline-primary text-dark">Read More</a>
                            </div>
                        </div>
                    </div>
				</div>
            </section>

            <section class="section-list-news">
                <div class="container beasty-wrapper">
                    <div class="latest-news row">
                        <div class="col-lg-4 col-md-12">
                            <div class="item-news" style="background: url(assets/images/article/Article1.jpg); background-size: cover;">
                                <div class="box-image">
                                    <img src="assets/images/article/Article1.jpg" class="img-fluid" />
                                </div>
                                <div class="content-news">
                                    <h5 class="title-news">Preparing your dog for<br/> a change  in routine</h5>
                                    <a href="article-detail.php" class="link-more">read more</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-12">
                            <div class="item-news" style="background: url(assets/images/article/Article2.jpg); background-size: cover;">
                                <div class="box-image">
                                    <img src="assets/images/article/Article2.jpg" class="img-fluid" />
                                </div>
                                <div class="content-news">
                                    <h5 class="title-news">A healthy dog is<br/> an active dog</h5>
                                    <a href="article-detail.php" class="link-more">read more</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-12">
                            <div class="item-news" style="background: url(assets/images/article/Article3.jpg); background-size: cover;">
                                <div class="box-image">
                                    <img src="assets/images/article/Article3.jpg" class="img-fluid" />
                                </div>
                                <div class="content-news">
                                    <h5 class="title-news">Heeping your best friend<br/> happy with beasty</h5>
                                    <a href="article-detail.php" class="link-more">read more</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-12">
                            <div class="item-news" style="background: url(assets/images/article/Article4.jpg); background-size: cover;">
                                <div class="box-image">
                                    <img src="assets/images/article/Article4.jpg" class="img-fluid" />
                                </div>
                                <div class="content-news">
                                    <h5 class="title-news">Help your dog reach <br/> their perfect weight</h5>
                                    <a href="article-detail.php" class="link-more">read more</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-12">
                            <div class="item-news" style="background: url(assets/images/article/Article5.jpg); background-size: cover;">
                                <div class="box-image">
                                    <img src="assets/images/article/Article5.jpg" class="img-fluid" />
                                </div>
                                <div class="content-news">
                                    <h5 class="title-news">How pets boost your mental <br/> and physical health</h5>
                                    <a href="article-detail.php" class="link-more">read more</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-12">
                            <div class="item-news" style="background: url(assets/images/article/Article6.jpg); background-size: cover;">
                                <div class="box-image">
                                    <img src="assets/images/article/Article6.jpg" class="img-fluid" />
                                </div>
                                <div class="content-news">
                                    <h5 class="title-news">How to teach your dog <br/> to do tricks</h5>
                                    <a href="article-detail.php" class="link-more">read more</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="box-pagination">
                        <ul class="pagination justify-content-center">
                            <li class="page-item disabled previous trigger-btn">
                                <a class="page-link" href="#"><</a>
                            </li>
                            <li class="page-item"><a class="page-link active" href="#">1</a></li>
                            <li class="page-item"><a class="page-link" href="#">2</a></li>
                            <li class="page-item"><a class="page-link" href="#">3</a></li>
                            <li class="page-item"><a class="page-link" href="#">4</a></li>
                            <li class="page-item"><a class="page-link" href="#">5</a></li>
                            <li class="page-item next trigger-btn">
                                <a class="page-link" href="#">></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </section>
			
			<?php include("partials/footer.php") ?>
		</div>

        <?php include("partials/script.php") ?>
        <script>
            $(document).ready(function(){
                $("#nanogallery2").nanogallery2( {
                    // ### gallery settings ### 
                    itemsBaseURL:     'http://localhost:3000/beasty-html/',

                    // GALLERY AND THUMBNAIL LAYOUT
                    thumbnailHeight: '350', thumbnailWidth: '350',
                    thumbnailAlignment: 'fillWidth',
                    galleryDisplayMode: 'fullContent',
                    thumbnailLabel: { display: false},

                      // GALLERY THEME
                        galleryTheme : { 
                            thumbnail: { borderColor: '#fff' }
                        },
                    
                });
            });
        </script>
	</body>
</html>