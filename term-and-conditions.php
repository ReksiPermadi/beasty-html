<!DOCTYPE html>
<html>
	<head>
		<title>Beasty - Term and Conditions</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, user-scalable=no">

		<?php include("partials/head.php") ?>
		
	</head>
	<body>
		<?php include("partials/top-navigation.php") ?>
		<?php include("partials/side-navigation.php") ?>
		<?php include("partials/menu-navigation.php") ?>

		<div class="main-content">
            <section class="section-tnc">
                <div class="box-head">
                    <div class="container beasty-wrapper">
                        <h2>Term and Conditions</h2>
                    </div>
                </div>
                <div class="container beasty-wrapper">
                    <div class="box-content">
                        <p>Help us create a beter Beasty for you.</p>
                        <p><br></p>
                        <p>To create a beter Beasty for you, we need your help. You can do this by providing us with various kinds of information to help us serve you beter, and ultimately improve Beasty for everyone. In the remaining portions of this document, you'll fnd an explanation of some of the reasons we request certain information from you, as well as details about how we obtain and use that information. Throughout this Privacy Policy document, areas where we refer to your "personal information" means any information you provide to us about you. This includes your name, postal address, email address, date of birth, telephone numbers, etc.<b><br></b></p>
                        <p><b>&nbsp;</b></p>
                        <p><b>Where we collect your personal information:</b><b><br><br></b></p>
                        <p><b>Customer relations</b><br>In order for a customer relations representative to respond to your questions or concerns, we may require certain personal information such as your name, email address, PayPal name and email address and/or telephone number. This information is used to answer your questions as completely and thoroughly as possible.<b><br></b></p>
                        <p><b>&nbsp;</b></p>
                        <p><b>Information that we collect automatically:</b><br>Many web surfers are concerned about their privacy and the use of "cookies" on the Internet. Cookies are small fles that can be used to store information you have already provided. Cookies cannot be usedto "steal" information about you or your computer system. The Beasty website uses cookies to remember your preferences. For example,<br>the website may remember which page you viewed, or the items added to your cart, to make your next visit more convenient. Nevertheless,cookies can be disabled in your web browser's Internet Options.<b><br></b></p>
                        <p><b>&nbsp;</b></p>
                        <p><b>Point of Sale</b></p>
                        <p>We will request frst time customers to provide their name, postal address, email address, telephone numbers for us to deliver the order to. This email address will be used solely to communicate with our customers and provide information on our latest promotions<b>.</b></p>
                        <p><b>&nbsp;</b></p>
                        <p><b>What we will do with your information:</b><br>Beasty does not and will not sell your personal information to third parties.<br>The personal data you provide to us will enable us to:</p>
                        <p>(a) track your purchase records,<br>(b) allocate points according to your purchase records,<br>(c) update you periodically about relevant promotions or new product launches,<br>(d) redeem your VIP points for discount codes on our website,<br>(e) update our profling of customer preferences so that we may be able to serve you beter.<b><br></b></p>
                        <p><b>&nbsp;</b></p>
                        <p><b>Marketing</b></p>
                        <p>By providing any personal information to us, you give us permission to email you for marketing purposes.We will only send you emails which may be of interest to you – for example, the shipping dates of your pre-order, to update your shipping address or&nbsp; information about new releases or discounts.</p>
                        <p>If you do not want us to send any marketing material to you, you can decide to opt-out of these types of marketing material at any time by changing the setings in your profle or by following the unsubscribe link at the botom of any email we send to you.<b><br></b></p>
                        <p><b>&nbsp;</b></p>
                        <p><b>Storing your information and keeping it up to date:</b></p>
                        <p>When you share your personal information with Beasty, you will always have access to this information. Where you store a "Profle" on the website, you can amend the shipping address details stored in your account yourself. If you wish to change the shipping details on your order, please drop us an email at <a href="mailto:beastypromise@beasty.com.au">beastypromise@beasty.com.au</a>.<b><br></b></p>
                        <p><b>&nbsp;</b></p>
                        <p><b>Keeping it safe</b></p>
                        <p>We are commited to protecting your personal information against foreseeable hazards. Making sure that your personal information is secure is very important to Beasty.<b><br></b></p>
                        <p><b>&nbsp;</b></p>
                        <p><b>Change of address? Change of personal information? Let us know:</b><br>Once you place an order, you log in to update your account contact number or address any time. If you wish to change the shipping details on your order, please drop us an email at <a href="mailto:beastypromise@beasty.com.au">beastypromise@beasty.com.au</a>.<br></p>
                        <p><br></p>
                        <p>If you would like us to update your personal information, remove your name from our mailing list, or if you have any questions about our privacy policy or your personal information we have on record, please contact us through our online request form here or email us at <a href="mailto:beastypromise@beasty.com.au">beastypromise@beasty.com.au</a>. We will respond to most questions or concerns<br>within 5-7 working days.</p>
                        <p><br></p>
                        <p>Occasionally, we may amend the details in this privacy policy and we advise our customers to review the privacy policy periodically to be updated of any changes.</p>
                        <p><br></p>
                        <p>By using this site, you consent to the above privacy policy.</p>
                    </div>
                </div>
            </section>
			
			<?php include("partials/footer.php") ?>
		</div>

        <?php include("partials/script.php") ?>
	</body>
</html>