<!DOCTYPE html>
<html>
	<head>
		<title>Beasty - My Account</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, user-scalable=no">

		<?php include("partials/head.php") ?>
		
	</head>
	<body>
		<?php include("partials/top-navigation-dark.php") ?>
		<?php include("partials/side-navigation.php") ?>
		<?php include("partials/menu-navigation.php") ?>

		<div class="main-content">
			<section class="shipping-list">
                <div class="container beasty-wrapper">
                    <div class="box-head">
                        <h3 class="title">Shipping Details</h3>
                    </div>
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Product Information</th>
                                    <th>Unit Price</th>
                                    <th>Quantity</th>
                                    <th>Subtotal</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>
                                        <div class="box-product">
                                            <div class="box-photo">
                                                <img src="assets/images/cart/Angus-Cuts-Cart.png" class="img-fluid" />
                                            </div>
                                            <p class="product-name">Angus Cuts</p>
                                        </div>
                                    </td>
                                    <td>$45.00</td>
                                    <td>1</td>
                                    <td>$45.00</td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="box-product">
                                            <div class="box-photo">
                                                <img src="assets/images/cart/Organ-Bites-Cart.png" class="img-fluid" />
                                            </div>
                                            <p class="product-name">Organ Bites</p>
                                        </div>
                                    </td>
                                    <td>$45.00</td>
                                    <td>2</td>
                                    <td>$90.00</td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="box-product">
                                            <div class="box-photo">
                                                <img src="assets/images/cart/Tendon-Chews-Cart.png" class="img-fluid" />
                                            </div>
                                            <p class="product-name">Tendon Chews</p>
                                        </div>
                                    </td>
                                    <td>$45.00</td>
                                    <td>2</td>
                                    <td>$90.00</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </section>

            <section class="shipping-progress">
                <div class="container beasty-wrapper">
                    <ul class="box-progress">
                        <li class="shipping-details active">Shipping Details</li>
                        <li class="payment-details">Payment Details</li>
                        <li class="order-confirmation">Order Confirmation</li>
                    </ul>
                    <div class="line"></div>
                </div>
            </section>

            <section class="shipping-order">
                <div class="container beasty-wrapper">
                    <div class="row">
                        <div class="col-lg-6 col-md-12">
                            <div class="shipping-address">
                                <h4 class="title">Shipping Address</h4>
                                <div class="box-order">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <p><b>Christopher Brian</b><br/>(+8075435432)</p>
                                        </div>
                                        <div class="col-md-6">
                                            <p>7/25 Adelaide St BrisbaneCity, Queensland, Australia</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="box-action-address">
                                    <a href="#"><i class="far fa-edit"></i></a>
                                    <div class="text-right">
                                        <a href="javascript:void(0)" data-toggle="modal" data-target="#modalAddAddress"><i class="fal fa-plus-circle"></i></a>
                                        <a href="javascript:void(0)" class="btn-show-address"><i class="fal fa-chevron-down"></i></a>
                                    </div>
                                </div>
                                <div class="box-address">
                                    <div class="box-order">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <label for="address1"><b>Christopher Brian</b><br/>(+8075435432)</label>
                                                <input type="radio" class="form-control radio" id="address1" name="address" checked />
                                            </div>
                                            <div class="col-md-6">
                                                <p>7/25 Adelaide St BrisbaneCity, Queensland, Australia</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="box-order">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <label for="address2"><b>Felix Brian Denise</b><br/>(+80752343245)</label>
                                                <input type="radio" class="form-control radio" id="address2" name="address" />
                                            </div>
                                            <div class="col-md-6">
                                                <p>10/30 Adelaide St BrisbaneCity, Queensland, Australia, 4210</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="box-order">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <label for="address3"><b>Nicholas Jake</b><br/>(+80751234245)</label>
                                                <input type="radio" class="form-control radio" id="address3" name="address" />
                                            </div>
                                            <div class="col-md-6">
                                                <p>20/25 Adelaide St BrisbaneCity, Queensland, Australia, 4120</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="payment-method" id="payment-method">
                                <h4 class="title">Payment Method</h4>
                                <div class="box-order box-head">
                                    <p>Credit Card Via</p>
                                    <img src="assets/images/payment/Stripe-Logo-Payment.png" class="img-fluid">
                                </div>
                                <div class="box-order">
                                    <div class="box-form">
                                        <form action="" class="form grey">
                                            <div class="form-group">
                                                <label>Card Holder Namer</label>
                                                <input type="text" class="form-control">
                                            </div>
                                            <div class="form-group">
                                                <label>Card Number</label>
                                                <input type="text" class="form-control">
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Expired Date</label>
                                                        <input type="text" class="form-control" />
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>CVV</label>
                                                        <input type="text" class="form-control" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="checkbox">
                                                <label><input type="checkbox" value="">Save payment information to my accountfor future purchases. </label>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                                <div class="box-note">
                                    <div class="list-note">
                                        <div class="checkbox">
                                            <label><input type="checkbox" value="">Save payment information to my accountfor future purchases. </label>
                                        </div>
                                        <p>Your personal data will be used to process you order, support your experience throughout this website and for other purposes described in our privacy policy.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-12 order-summary">
                            <h4 class="title">Order Summary</h4>
                            <div class="box-order">
                                <div class="row">
                                    <div class="col-md-6 col-6">
                                        <p class="label">Items Subtotal</p>
                                    </div>
                                    <div class="col-md-6 col-6 text-right">
                                        <p>$ 225.00</p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6 col-6">
                                        <p class="label">Shipping</p>
                                    </div>
                                    <div class="col-md-6 col-6 text-right">
                                        <p>$ 0.00</p>
                                    </div>
                                </div>
                            </div>
                            <div class="box-order">
                                <div class="row">
                                    <div class="col-md-6 col-6">
                                        <p class="label"><b>Total</b></p>
                                    </div>
                                    <div class="col-md-6 col-6 text-right">
                                        <p><b>$ 225.00</b></p>
                                    </div>
                                </div>
                            </div>
                            <a href="javascript:void(0)" class="btn btn-checkout btn-payment-method">Proceed to checkout</a>
                            <a href="javascript:void(0)" class="btn btn-checkout btn-payment-modal" data-toggle="modal" data-target="#modalPaymentSuccess">Proceed to checkout</a>
                            <div class="box-note">
                                <div class="list-note">
                                    <p><b>Confirmation</b><p>
                                    <p>After purchasing a Beasty product we will send a confirmation message with delivery information and custom message template</p>
                                </div>
                                <div class="list-note">
                                    <p><b>Shipping and return policy</b><p>
                                    <p>We have a no refund policy. If goods are damaged during shipping we will replace and ship a new one free of charge.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
			
            <?php include("partials/footer.php") ?>
		</div>

        <div id="modalAddAddress" class="modal fade modal-address" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-body">
                        <button type="button" class="close" data-dismiss="modal"><i class="fal fa-times"></i></button>
                        
                        <div class="box-content">
                            <div class="box-head">
                                <p class="title-form">Add New Address</p>
                            </div>

                            <div class="box-form">
                                <form action="" class="form grey">
                                    <div class="form-group">
                                        <label>Name</label>
                                        <input type="text" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label>Phone Number</label>
                                        <input type="text" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label for="name">Name</label>
                                        <input type="text" class="form-control" id="name">
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Country</label>
                                                <input type="text" class="form-control" />
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Region</label>
                                                <select class="form-control">
                                                    <option value="0">Select Region</option>
                                                    <option value="1">Queensland</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Postal Code</label>
                                                <input type="text" class="form-control" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>Address</label>
                                        <textarea class="form-control"></textarea>
                                    </div>
                                    <div class="checkbox">
                                        <label><input type="checkbox" value="">Set as default address</label>
                                    </div>
                                    <div class="text-center btn-holder">
                                        <button type="button" class="btn btn-outline-primary text-dark">Add New Address</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>

        <!-- Modal Payment Success -->
        <div id="modalPaymentSuccess" class="modal fade modal-signin modal-success modal-payment-success" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-body">
                        <button type="button" class="close" data-dismiss="modal"><i class="fal fa-times"></i></button>
                        <div class="box-head">
                            <p class="title-form">Thank You</p>
                            <p class="title-form orange">For Your Order</p>
                        </div>
                        <div class="box-head small">
                            <p class="title-form">You successfully made </p>
                            <p class="title-form">a payment with Stripe.</p>
                        </div>
                        <div class="text-center btn-holder">
                            <a href="order-confirmation.php" class="btn btn-outline-primary text-dark">Order Confirmation</a>
                        </div>
                        <div class="box-image">
                            <img src="assets/images/payment/Success-Payment.png" class="img-fluid" />
                        </div>
                    </div>
                </div>

            </div>
        </div>

        <!-- Modal Payment Failed -->
        <div id="modalPaymentFailed" class="modal fade modal-signin modal-success modal-payment-success" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-body">
                        <button type="button" class="close" data-dismiss="modal"><i class="fal fa-times"></i></button>
                        <div class="box-head">
                            <p class="title-form orange">Opps, Sorry</p>
                            <p class="title-form">Payment Failed</p>
                        </div>
                        <div class="box-head small">
                            <p class="title-form">It looks like your order could not be proceed at this time. Please try again or select a different payment option</p>
                        </div>
                        <div class="text-center btn-holder">
                            <a href="#" class="btn btn-outline-primary text-dark">Try Again</a>
                        </div>
                    </div>
                </div>

            </div>
        </div>

        <?php include("partials/script.php") ?>

        <script>
                $(document).ready(function(){
                    $('.btn-show-address').click(function(){
                        $('.box-address').slideToggle();
                    });

                    $('.btn-payment-method').click(function(){
                        $('.shipping-address').hide();
                        $('.payment-method').fadeIn();
                        $('html, body').animate({
                            scrollTop: $("#payment-method").offset().top - 100
                        }, 500);
                        $('.payment-details').addClass('active');
                        $(this).hide();
                        $('.btn-payment-modal').css('display', 'block');
                    });

                    $('.shipping-details').click(function() {
                        $('.payment-method').hide();
                        $('.shipping-address').fadeIn();
                        $('.payment-details').removeClass('active');
                        $('.btn-payment-modal').hide();
                        $('.btn-payment-method').css('display', 'block');
                    });
                });
        </script>
	</body>
</html>