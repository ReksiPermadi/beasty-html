<!DOCTYPE html>
<html>
	<head>
		<title>Beasty - Gallery</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, user-scalable=no">

		<?php include("partials/head.php") ?>
		
	</head>
	<body>
		<?php include("partials/top-navigation.php") ?>
		<?php include("partials/side-navigation.php") ?>
		<?php include("partials/menu-navigation.php") ?>

		<div class="main-content">
			<section class="section-banner-gallery">
				<div class="container beasty-wrapper">
					<div class="row">
                        <div class="col-lg-6 col-md-12 order-lg-first order-last">
                            <div class="img-banner">
                                <img src="assets/images/instagram-gallery/Dog-Gallery.png" class="img-fluid"/>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-12 order-lg-last order-first">
                            <div class="content-banner">
                                <div class="small-title">
                                    <h3>Instagram</h3>
                                </div>
                                <div class="big-title">
                                    <h1>Gallery</h1>
                                </div>
                            </div>
                        </div>
                    </div>
				</div>
            </section>
            
            <section class="section-gallery">
                <div class="container beasty-wrapper">
                    <div id="nanogallery2" class="box-gallery">
                        <a href="assets/images/instagram-gallery/IG-Post-1-Gallery.jpg"  data-ngThumb="assets/images/instagram-gallery/IG-Post-1-Gallery.jpg">Image 1</a>
                        <a href="assets/images/instagram-gallery/IG-Post-2-Gallery.jpg"  data-ngThumb="assets/images/instagram-gallery/IG-Post-2-Gallery.jpg">Image 2</a>
                        <a href="assets/images/instagram-gallery/IG-Post-3-Gallery.jpg"  data-ngThumb="assets/images/instagram-gallery/IG-Post-3-Gallery.jpg">Image 3</a>

                        <a href="assets/images/instagram-gallery/IG-Post-4-Gallery.jpg"  data-ngThumb="assets/images/instagram-gallery/IG-Post-4-Gallery.jpg">Image 4</a>
                        <a href="assets/images/instagram-gallery/IG-Post-5-Gallery.jpg"  data-ngThumb="assets/images/instagram-gallery/IG-Post-5-Gallery.jpg">Image 5</a>
                        <a href="assets/images/instagram-gallery/IG-Post-6-Gallery.jpg"  data-ngThumb="assets/images/instagram-gallery/IG-Post-6-Gallery.jpg">Image 6</a>

                        <a href="assets/images/instagram-gallery/IG-Post-7-Gallery.jpg"  data-ngThumb="assets/images/instagram-gallery/IG-Post-7-Gallery.jpg">Image 4</a>
                        <a href="assets/images/instagram-gallery/IG-Post-8-Gallery.jpg"  data-ngThumb="assets/images/instagram-gallery/IG-Post-8-Gallery.jpg">Image 5</a>
                        <a href="assets/images/instagram-gallery/IG-Post-9-Gallery.jpg"  data-ngThumb="assets/images/instagram-gallery/IG-Post-9-Gallery.jpg">Image 6</a>
                    </div>
                    <div class="text-center mt-5">
                        <a href="#" class="btn btn-outline-primary text-dark">View More</a>
                    </div>
                </div>
            </section>
			
			<?php include("partials/footer.php") ?>
		</div>

        <?php include("partials/script.php") ?>
        <script>
            $(document).ready(function(){
                $("#nanogallery2").nanogallery2( {
                    // ### gallery settings ### 
                    itemsBaseURL:     'http://localhost:3000/beasty-html/',

                    // GALLERY AND THUMBNAIL LAYOUT
                    thumbnailHeight: '350', thumbnailWidth: '350',
                    thumbnailAlignment: 'fillWidth',
                    galleryDisplayMode: 'fullContent',
                    thumbnailLabel: { display: false},

                      // GALLERY THEME
                        galleryTheme : { 
                            thumbnail: { borderColor: '#fff' }
                        },
                    
                });

                $('.menu-desktop .account-menu img').attr('src','assets/images/side-menu/Account-Icon-Dark.svg');
                $('.menu-desktop .shopping-cart-menu img').attr('src','assets/images/side-menu/Shopping-Cart-Icon-Dark.svg');
                $('.menu-desktop .product-menu img').attr('src','assets/images/side-menu/Product-Catalogue-Icon-Dark.svg');
                $('.menu-desktop .our-story-menu img').attr('src','assets/images/side-menu/Our-Story-Icon-Dark.svg');
                
                $('.menu-mobile .account-menu img').attr('src','assets/images/side-menu/Account-Icon.svg');
                $('.menu-mobile .shopping-cart-menu img').attr('src','assets/images/side-menu/Shopping-Cart-Icon.svg');
                $('.menu-mobile .product-menu img').attr('src','assets/images/side-menu/Product-Catalogue-Icon.svg');
                $('.menu-mobile .our-story-menu img').attr('src','assets/images/side-menu/Our-Story-Icon.svg');
                $('.menu-icon').addClass('color-white');
                $('.img-logo').attr('src','assets/images/White-Logo.svg');
            });

            $(window).scroll(function(){
                var banner = $('.section-banner-gallery').height();
                var top = $(window).scrollTop() + $('.box-sidebar').height() + 30;
                var logo = $('.img-logo');
                var topLogo = 200;
                if ($(window).scrollTop() >= topLogo) {
                    logo.attr('src','assets/images/Beasty-Logo.svg');
                    $('.menu-mobile .account-menu img').attr('src','assets/images/side-menu/Account-Icon-Dark.svg');
                    $('.menu-mobile .shopping-cart-menu img').attr('src','assets/images/side-menu/Shopping-Cart-Icon-Dark.svg');
                    $('.menu-mobile .product-menu img').attr('src','assets/images/side-menu/Product-Catalogue-Icon-Dark.svg');
                    $('.menu-mobile .our-story-menu img').attr('src','assets/images/side-menu/Our-Story-Icon-Dark.svg');
                } else {
                    logo.attr('src','assets/images/White-Logo.svg');
                    $('.menu-mobile .account-menu img').attr('src','assets/images/side-menu/Account-Icon.svg');
                    $('.menu-mobile .shopping-cart-menu img').attr('src','assets/images/side-menu/Shopping-Cart-Icon.svg');
                    $('.menu-mobile .product-menu img').attr('src','assets/images/side-menu/Product-Catalogue-Icon.svg');
                    $('.menu-mobile .our-story-menu img').attr('src','assets/images/side-menu/Our-Story-Icon.svg');
                }

                if (top > banner) {
                    $('.menu-desktop .account-menu img').attr('src','assets/images/side-menu/Account-Icon.svg');
                    $('.menu-desktop .shopping-cart-menu img').attr('src','assets/images/side-menu/Shopping-Cart-Icon.svg');
                    $('.menu-desktop .product-menu img').attr('src','assets/images/side-menu/Product-Catalogue-Icon.svg');
                    $('.menu-desktop .our-story-menu img').attr('src','assets/images/side-menu/Our-Story-Icon.svg');
                } else {
                    $('.menu-desktop .account-menu img').attr('src','assets/images/side-menu/Account-Icon-Dark.svg');
                    $('.menu-desktop .shopping-cart-menu img').attr('src','assets/images/side-menu/Shopping-Cart-Icon-Dark.svg');
                    $('.menu-desktop .product-menu img').attr('src','assets/images/side-menu/Product-Catalogue-Icon-Dark.svg');
                    $('.menu-desktop .our-story-menu img').attr('src','assets/images/side-menu/Our-Story-Icon-Dark.svg');
                }
            });
        </script>
	</body>
</html>